<?php

function current_version()
{
    $current_version = '3.4.0';
    return $current_version;
}

function transparentmaps_install_addon()
{
    $current_version = current_version();
    global $conn, $config, $misc;
    require_once($config['basepath'] . '/include/misc.inc.php');
    $misc = new Misc();
    // Check Current Installed Version
    $sql = 'SELECT addons_version 
			FROM ' . $config['table_prefix_no_lang'] . 'addons 
			WHERE addons_name = \'transparentmaps\'';
    $recordSet = $conn->Execute($sql);
    $version = $recordSet->fields(0);
    if ($version == '') {
        // Preform a new install. Create any needed databases etc, and insert version number into addon table.
        $sql_insert[] = "INSERT INTO " . $config["table_prefix_no_lang"] . "addons (addons_version, addons_name) VALUES ('" . $current_version . "', 'transparentmaps')";
        $sql_insert[] = "CREATE TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps (
			addons_transparentmaps_id INT(5) NOT NULL AUTO_INCREMENT,
			addons_transparentmaps_apikey CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_show_address INT(2) NOT NULL,
			addons_transparentmaps_show_city INT(2) NOT NULL,
			addons_transparentmaps_show_state INT(2) NOT NULL,
			addons_transparentmaps_show_zip INT(2) NOT NULL,
			addons_transparentmaps_show_photo INT(2) NOT NULL,
			addons_transparentmaps_show_title INT(2) NOT NULL,
			addons_transparentmaps_field_1 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_field_2 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_field_3 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_field_4 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_field_5 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_field_6 CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_caption_field_1 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_caption_field_2 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_caption_field_3 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_caption_field_4 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_caption_field_5 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_caption_field_6 CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_text_0 TEXT NOT NULL,
			addons_transparentmaps_text_1 TEXT NOT NULL,
			addons_transparentmaps_text_2 TEXT NOT NULL,
			addons_transparentmaps_text_3 TEXT NOT NULL,
			addons_transparentmaps_text_4 TEXT NOT NULL,
			addons_transparentmaps_text_5 TEXT NOT NULL,
			addons_transparentmaps_text_6 TEXT NOT NULL,
			addons_transparentmaps_search_results_popup INT(2) NOT NULL,
			addons_transparentmaps_search_results_width INT(4) NOT NULL,
			addons_transparentmaps_search_results_height INT(4) NOT NULL,
			addons_transparentmaps_search_results_smmapcontrol INT(4) NOT NULL,
			addons_transparentmaps_search_results_mapttype TEXT NOT NULL,
			addons_transparentmaps_search_results_scale INT(4) NOT NULL,
			addons_transparentmaps_showmap_popup INT(2) NOT NULL,
			addons_transparentmaps_showmap_width INT(4) NOT NULL,
			addons_transparentmaps_showmap_height INT(4) NOT NULL,
			addons_transparentmaps_showmap_smmapcontrol INT(4) NOT NULL,
			addons_transparentmaps_showmap_mapttype TEXT NOT NULL,
			addons_transparentmaps_showmap_scale INT(4) NOT NULL,
			addons_transparentmaps_area_popup INT(2) NOT NULL,
			addons_transparentmaps_area_width INT(4) NOT NULL,
			addons_transparentmaps_area_height INT(4) NOT NULL,
			addons_transparentmaps_area_smmapcontrol INT(4) NOT NULL,
			addons_transparentmaps_area_mapttype TEXT NOT NULL,
			addons_transparentmaps_area_scale INT(4) NOT NULL,
			addons_transparentmaps_area_distance DECIMAL(4,2) NOT NULL,
			addons_transparentmaps_area_zoom INT(4) NOT NULL,
			addons_transparentmaps_user_popup INT(2) NOT NULL,
			addons_transparentmaps_user_width INT(4) NOT NULL,
			addons_transparentmaps_user_height INT(4) NOT NULL,
			addons_transparentmaps_user_smmapcontrol INT(4) NOT NULL,
			addons_transparentmaps_user_mapttype TEXT NOT NULL,
			addons_transparentmaps_user_scale INT(4) NOT NULL,
			addons_transparentmaps_default_country CHAR VARYING(45) NOT NULL,
			addons_transparentmaps_yahookey CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_area_allclasses INT(2) NOT NULL,
			addons_transparentmaps_otherkey CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_showall_popup INT(2) NOT NULL,
			addons_transparentmaps_showall_width INT(4) NOT NULL,
			addons_transparentmaps_showall_height INT(4) NOT NULL,
			addons_transparentmaps_showall_smmapcontrol INT(4) NOT NULL,
			addons_transparentmaps_showall_mapttype TEXT NOT NULL,
			addons_transparentmaps_showall_scale INT(4) NOT NULL,
			addons_transparentmaps_use_customurl INT(2) NOT NULL,
			addons_transparentmaps_customurl CHAR VARYING(255) NOT NULL,
			addons_transparentmaps_default_lat FLOAT(10,6) NOT NULL,
			addons_transparentmaps_default_long FLOAT(10,6) NOT NULL,
			addons_transparentmaps_default_zoom INT(4) NOT NULL,
			addons_transparentmaps_showall_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_search_results_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_showmap_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_area_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_user_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_user_localsearch INT(2) NOT NULL,
			addons_transparentmaps_user_directions INT(2) NOT NULL,
			addons_transparentmaps_user_streetview INT(2) NOT NULL,
			addons_transparentmaps_user_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_showall_localsearch INT(2) NOT NULL,
			addons_transparentmaps_showall_directions INT(2) NOT NULL,
			addons_transparentmaps_showall_streetview INT(2) NOT NULL,
			addons_transparentmaps_showall_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_popup INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_width INT(4) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_height INT(4) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_smmapcontrol INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_scale INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_listingmapsearchresult_localsearch INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_directions INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_streetview INT(2) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_areasearchresult_popup INT(2) NOT NULL,
			addons_transparentmaps_areasearchresult_width INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_height INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_smmapcontrol INT(2) NOT NULL,
			addons_transparentmaps_areasearchresult_mapttype TEXT NOT NULL,
			addons_transparentmaps_areasearchresult_scale INT(2) NOT NULL,
			addons_transparentmaps_areasearchresult_distance DECIMAL(4,2) NOT NULL,
			addons_transparentmaps_areasearchresult_zoom INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_allclasses INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP',
			addons_transparentmaps_areasearchresult_localsearch INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_directions INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_streetview INT(4) NOT NULL,
			addons_transparentmaps_areasearchresult_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_google_geocoder_priority INT(4) NOT NULL,
			addons_transparentmaps_yahoo_geocoder_priority INT(4) NOT NULL,
			addons_transparentmaps_other_geocoder_priority INT(4) NOT NULL,
			addons_transparentmaps_scroll_wheel_zoom INT(4) NOT NULL,
			addons_transparentmaps_continuous_zoom INT(4) NOT NULL,
			addons_transparentmaps_enable_dragging INT(4) NOT NULL,
			addons_transparentmaps_search_results_localsearch INT(4) NOT NULL,
			addons_transparentmaps_search_results_directions INT(4) NOT NULL,
			addons_transparentmaps_search_results_streetview INT(4) NOT NULL,
			addons_transparentmaps_search_results_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_showmap_localsearch INT(4) NOT NULL,
			addons_transparentmaps_showmap_directions INT(4) NOT NULL,
			addons_transparentmaps_showmap_streetview INT(4) NOT NULL,
			addons_transparentmaps_showmap_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_area_localsearch INT(4) NOT NULL,
			addons_transparentmaps_area_directions INT(4) NOT NULL,
			addons_transparentmaps_area_streetview INT(4) NOT NULL,
			addons_transparentmaps_area_streetviewpano CHAR VARYING(20) NOT NULL,
			addons_transparentmaps_listingmapsearchresult_mapttype TEXT NOT NULL,
			addons_transparentmaps_search_results_klmoverlay TEXT NULL,
			addons_transparentmaps_showmap_klmoverlay TEXT NULL,
			addons_transparentmaps_area_klmoverlay TEXT NULL,
			addons_transparentmaps_user_klmoverlay TEXT NULL,
			addons_transparentmaps_showall_klmoverlay TEXT NULL,
			addons_transparentmaps_listingmapsearchresult_klmoverlay TEXT NULL,
			addons_transparentmaps_areasearchresult_klmoverlay TEXT NULL,
			addons_transparentmaps_icon_width INT(4) NOT NULL,
			addons_transparentmaps_icon_height INT(4) NOT NULL,
			addons_transparentmaps_shadow_width INT(4) NOT NULL,
			addons_transparentmaps_shadow_height INT(4) NOT NULL,
			addons_transparentmaps_showmap_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_areasearchresult_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_listingmapsearchresult_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_search_results_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_area_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_user_autozoom varchar(4) NOT NULL DEFAULT 'az',
			addons_transparentmaps_showall_autozoom varchar(4) NOT NULL DEFAULT 'az',

			PRIMARY KEY(addons_transparentmaps_id)
		)";

        $sql_insert[] = "INSERT INTO " . $config['table_prefix_no_lang'] . "addons_transparentmaps VALUES ('1','enter Google API key','1','0','0','0','1','1','','','','','','','both','both','both','both','both','both','','','','','','','','1','600','600','0','1','1','1','600','600','0','G_NORMAL_MAP','1','1','600','600','0','G_NORMAL_MAP','1','1','14','1','600','600','0','G_NORMAL_MAP','1','USA','enter Yahoo! App ID','1','Alternate Geocoder Key','1','600','600','0','1','1','0','option=com_cmsrealty&Itemid=1','39.368279','-100.195312','4','G_NORMAL_MAP','G_NORMAL_MAP','G_NORMAL_MAP','G_NORMAL_MAP','G_NORMAL_MAP','0','0','0','NONE','0','0','0','NONE','0','600','600','0','0','G_NORMAL_MAP','0','0','0','NONE','0','600','600','0','G_NORMAL_MAP','0','1.00','0','0','G_NORMAL_MAP','0','0','0','NONE','1','2','0','0','0','1','0','0','0','NONE','0','0','0','NONE','0','0','0','NONE','G_NORMAL_MAP','','','','','','','',12,20,22,20,'az','az','az','az','az','az','az')";
        // CHECK FOR LATITUDE AND LONGITUDE FIELD NAMES
        // Get Name of Lat Field
        $sql = "SELECT listingsformelements_field_name FROM " . $config['table_prefix'] . "listingsformelements WHERE listingsformelements_field_type  = 'lat'";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $num_lat_fields = $recordSet->RecordCount();
        // IF WE DON'T HAVE A LATITUDE FIELD THEN ADD ONE
        if ($num_lat_fields == 0) {
            $sql_insert[] = "INSERT INTO " . $config['table_prefix'] . "listingsformelements (listingsformelements_field_type, listingsformelements_field_name, listingsformelements_field_caption, listingsformelements_default_text, listingsformelements_field_elements, listingsformelements_rank, listingsformelements_search_rank, listingsformelements_search_result_rank, listingsformelements_required, listingsformelements_location, listingsformelements_display_on_browse, listingsformelements_search_step, listingsformelements_searchable, listingsformelements_search_label, listingsformelements_search_type,listingsformelements_display_priv) VALUES ('lat','transparentmaps_latitude','Latitude','','','0','20','20','No','','No','0','0','','','0')";
        }
        // Get Name of Long Field
        $sql = "SELECT listingsformelements_field_name FROM " . $config['table_prefix'] . "listingsformelements WHERE listingsformelements_field_type  = 'long'";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $num_long_fields = $recordSet->RecordCount();
        // IF WE DON'T HAVE A LONGITUDE FIELD THEN ADD ONE
        if ($num_long_fields == 0) {
            $sql_insert[] = "INSERT INTO " . $config['table_prefix'] . "listingsformelements (listingsformelements_field_type, listingsformelements_field_name, listingsformelements_field_caption, listingsformelements_default_text, listingsformelements_field_elements, listingsformelements_rank, listingsformelements_search_rank, listingsformelements_search_result_rank, listingsformelements_required, listingsformelements_location, listingsformelements_display_on_browse, listingsformelements_search_step, listingsformelements_searchable, listingsformelements_search_label, listingsformelements_search_type,listingsformelements_display_priv) VALUES ('long','transparentmaps_longitude','Longitude','','','0','20','20','No','','No','0','0','','','0')";
        }
        //// END CHECK FOR LATITUDE AND LONGITUDE FIELD NAMES
        //// RUN INSTALL INSERT STUFF NOW
        foreach ($sql_insert as $key => $val) {
            $recordSet = $conn->Execute($val);
            if (!$recordSet) {
                die("<b><font color=red>ERROR - $val</font></b>");
            }
        }
        //// NOW ADD THE LATITUDE FIELD TO EXISTING LISTINGS AND PROPERTY CLASSES
        if ($num_lat_fields == 0) {
            // Now we need to get the field ID
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name 
					FROM ' . $config['table_prefix'] . 'listingsformelements 
					WHERE (listingsformelements_field_name = \'transparentmaps_latitude\')';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $listingsformelements_id = $recordSet->fields('listingsformelements_id');
            // We should now add a blank field for each listing that already exist.
            $sql = 'SELECT listingsdb_id, userdb_id FROM ' . $config['table_prefix'] . 'listingsdb';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }

            $id = [];
            $user = [];
            while (!$recordSet->EOF) {
                $id[] = $recordSet->fields('listingsdb_id');
                $user[] = $recordSet->fields('userdb_id');
                $recordSet->MoveNext();
            } // while

            $count = count($id);
            $x = 0;
            while ($x < $count) {
                $sql = "INSERT INTO " . $config['table_prefix'] . "listingsdbelements (listingsdbelements_field_name, listingsdb_id, userdb_id, listingsdbelements_field_value) 
						VALUES ('transparentmaps_latitude',$id[$x],$user[$x],'')";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $x++;
            }
            $class_id = [];
            $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'class 
					ORDER BY class_rank';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            while (!$recordSet->EOF) {
                $class_id[] = $misc->make_db_unsafe($recordSet->fields('class_id'));
                $recordSet->MoveNext();
            }
            // Add Listing Field to ALL property classes
            foreach ($class_id as $id) {
                $sql = 'INSERT INTO ' . $config['table_prefix_no_lang'] . 'classformelements (class_id,listingsformelements_id) 
						VALUES (' . $id . ',' . $listingsformelements_id . ')';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
            }
        }
        ////NOW ADD THE LONGITUDE FIELD TO EXISTING LISTINGS AND PROPERTY CLASSES
        if ($num_long_fields == 0) {
            // Now we need to get the field ID
            $sql = 'SELECT listingsformelements_id, listingsformelements_field_name 
					FROM ' . $config['table_prefix'] . 'listingsformelements 
					WHERE (listingsformelements_field_name = \'transparentmaps_longitude\')';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $listingsformelements_id = $recordSet->fields('listingsformelements_id');
            // We should now add a blank field for each listing that already exist.
            $sql = 'SELECT listingsdb_id, userdb_id FROM ' . $config['table_prefix'] . 'listingsdb';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            $id = [];
            $user = [];
            while (!$recordSet->EOF) {
                $id[] = $recordSet->fields('listingsdb_id');
                $user[] = $recordSet->fields('userdb_id');
                $recordSet->MoveNext();
            } // while
            $count = count($id);
            $x = 0;
            while ($x < $count) {
                $sql = "INSERT INTO " . $config['table_prefix'] . "listingsdbelements (listingsdbelements_field_name, listingsdb_id, userdb_id, listingsdbelements_field_value) 
						VALUES ('transparentmaps_longitude',$id[$x],$user[$x],'')";
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
                $x++;
            }
            // Add Listing Field to property class
            $class_id = [];
            $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'class ORDER BY class_rank';
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            while (!$recordSet->EOF) {
                $class_id[] = $misc->make_db_unsafe($recordSet->fields('class_id'));
                $recordSet->MoveNext();
            }
            foreach ($class_id as $id) {
                $sql = 'INSERT INTO ' . $config['table_prefix_no_lang'] . 'classformelements (class_id,listingsformelements_id) 
						VALUES (' . $id . ',' . $listingsformelements_id . ')';
                $recordSet = $conn->Execute($sql);
                if (!$recordSet) {
                    $misc->log_error($sql);
                }
            }
        }
    } elseif ($version != $current_version) {
        // Perform Updates to database based on previous installed version.
        switch ($version) {
            case '0':
            case '1':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps DROP COLUMN addons_transparentmaps_yahookey";
                // no break
            case '1.0.1':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_default_country CHAR VARYING(45) NOT NULL DEFAULT 'USA'";
                // no break
            case '1.0.2':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_yahookey CHAR VARYING(255) NOT NULL DEFAULT 'Enter Yahoo! App ID'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_allclasses INT(2) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_geocoder CHAR VARYING(20) NOT NULL DEFAULT 'google_geocoder'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_otherkey CHAR VARYING(255) NOT NULL DEFAULT 'Alternate Geocoder Key'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_popup INT(2) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_width INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_height INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_smmapcontrol INT(2) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_mapttype INT(2) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_scale INT(2) NOT NULL DEFAULT '1'";
                // no break
            case '1.1.1':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_use_customurl INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_customurl CHAR VARYING(255) NOT NULL DEFAULT 'option=com_cmsrealty&Itemid=1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_default_lat FLOAT(10,6) NOT NULL DEFAULT '39.368279'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_default_long FLOAT(10,6) NOT NULL DEFAULT '-100.195312'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_default_zoom INT(4) NOT NULL DEFAULT '4'";
                // no break
            case '1.1.2':
            case '1.1.3':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_default_maptype CHAR VARYING(20) NOT NULL DEFAULT 'G_NORMAL_MAP'";
                // no break
            case '1.1.4':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_localsearch INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_directions INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_streetview INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_localsearch INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_directions INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_streetview INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_popup INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_width INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_height INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_smmapcontrol INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_scale INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_default_maptype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_localsearch INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_directions INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_streetview INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_popup INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_width INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_height INT(4) NOT NULL DEFAULT '600'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_smmapcontrol INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_scale INT(2) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_distance DECIMAL(4,2) NOT NULL DEFAULT '1.00'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_zoom INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_allclasses INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_default_maptype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_localsearch INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_directions INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_streetview INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_google_geocoder_priority INT(4) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_yahoo_geocoder_priority INT(4) NOT NULL DEFAULT '2'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_other_geocoder_priority INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_scroll_wheel_zoom INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_continuous_zoom INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_enable_dragging INT(4) NOT NULL DEFAULT '1'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_localsearch INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_directions INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_streetview INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_localsearch INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_directions INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_streetview INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_localsearch INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_directions INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_streetview INT(4) NOT NULL DEFAULT '0'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_streetviewpano CHAR VARYING(20) NOT NULL DEFAULT 'NONE'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_0 addons_transparentmaps_text_0 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_1 addons_transparentmaps_text_1 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_2 addons_transparentmaps_text_2 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_3 addons_transparentmaps_text_3 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_4 addons_transparentmaps_text_4 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_5 addons_transparentmaps_text_5 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_text_6 addons_transparentmaps_text_6 TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_area_mapttype addons_transparentmaps_area_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_user_mapttype addons_transparentmaps_user_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_showall_mapttype addons_transparentmaps_showall_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_search_results_mapttype addons_transparentmaps_search_results_mapttype TEXT NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps CHANGE COLUMN addons_transparentmaps_showmap_mapttype addons_transparentmaps_showmap_mapttype TEXT NOT NULL";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_area_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_user_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_showall_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_search_results_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_showmap_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_areasearchresult_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_listingmapsearchresult_mapttype = 'G_NORMAL_MAP'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_klmoverlay TEXT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_icon_width INT(4) NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_icon_height INT(4) NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_shadow_width INT(4) NOT NULL";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_shadow_height INT(4) NOT NULL";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_icon_width = '12'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_icon_height = '20'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_shadow_width = '22'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_shadow_height = '20'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_listingmapsearchresult_width = '400'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_listingmapsearchresult_height = '400'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_areasearchresult_width = '400'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_areasearchresult_height = '400'";
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons_transparentmaps SET addons_transparentmaps_areasearchresult_distance = '1.00'";
                // no break
            case '1.2.0':
            case '1.2.1':
            case '1.2.2':
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showmap_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_areasearchresult_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_listingmapsearchresult_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_search_results_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_area_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_user_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                $sql_insert[] = "ALTER TABLE " . $config['table_prefix_no_lang'] . "addons_transparentmaps ADD COLUMN addons_transparentmaps_showall_autozoom varchar(4) NOT NULL DEFAULT 'az'";
                // no break
            case '1.2.6':
            case '2.1.0':
            case '2.1.4':
            case '2.1.6':
            case '2.2.1':
            case '3.1.0beta1':
            case '3.1.0beta2':
            case '3.1.0beta4':
            case '3.1.0':
            case '3.1.1':
            case '3.1.2':
            case '3.2':
            case '3.3.0-beta1':
            case '3.3.0':
            case '3.3.1':
            case '3.3.2':
            case '3.3.3':
            case '3.4.0-beta.1':
            default:
                // Update version
                $sql_insert[] = "UPDATE  " . $config['table_prefix_no_lang'] . "addons 
							SET addons_version ='" . $current_version . "' 
							WHERE addons_name = 'transparentmaps'";
                break;
        }
        // End of version upgrade check switch
        foreach ($sql_insert as $elementContents) {
            $recordSet = $conn->Execute($elementContents);
            if (!$recordSet) {
                if ($_SESSION['devel_mode'] == 'no') {
                    die("<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />");
                } else {
                    echo "<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />";
                }
            }
        }
        return true;
    }
    return false;
}

function transparentmaps_show_admin_icons()
{
    global $config, $lang;
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $admin_links = [];
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $can_edit_config = $login->loginCheck('edit_site_config', true);
    $can_edit_all_listings = $login->loginCheck('edit_all_listings', true);
    if ($can_edit_config === true) {
        $admin_links[] = '<a href="index.php?action=addon_transparentmaps_admin"><img border="0" src="' . $config['baseurl'] . '/addons/transparentmaps/images/transparentmaps.gif" /><br />' . $lang['tmaps_admin'] . '</a>';
    }
    if ($can_edit_all_listings === true) {
        $admin_links[] = '<a href="index.php?action=addon_transparentmaps_geocode_all"><img border="0" src="' . $config['baseurl'] . '/addons/transparentmaps/images/geocode.gif" /><br />' . $lang['tmaps_geocodeall'] . '</a>';
    }
    return $admin_links;
}

function transparentmaps_load_template()
{
    $template_array = [
        'addon_transparentmaps_loadgoogleapis', 'addon_transparentmaps_preloadicons',
        'addon_transparentmaps_search_results', 'addon_transparentmaps_showmap_searchresults',
        'addon_transparentmaps_showmap_searchresults_link', 'addon_transparentmaps_area_searchresults',
        'addon_transparentmaps_area_searchresults_link', 'addon_transparentmaps_showmap',
        'addon_transparentmaps_showmap_link', 'addon_transparentmaps_area',
        'addon_transparentmaps_area_link', 'addon_transparentmaps_user',
        'addon_transparentmaps_user_link', 'addon_transparentmaps_showall',
        'addon_transparentmaps_showall_link',
    ];
    return $template_array;
}

function transparentmaps_addonmanager_help()
{
    $template_tags = [];
    $action_urls = [];
    $doc_url = 'https://docs.open-realty.org/nav.addons/02.transparentMaps/';
    $template_tags['addon_transparentmaps_loadgoogleapis'] = 'Main Template File // Printer Friendly <b>- REQUIRED TAG FOR ALL MAPS -</b> This tag loads the REQUIRED code for the Google Maps API. This tag MUST be placed in the <head></head> tags of the template main.html file where the map will be called. If using the popup maps you must place this tag in the <head></head> of the printer_friendly.html template file. ';
    $template_tags['addon_transparentmaps_preloadicons'] = 'Main Template File // Printer Friendly - Embeds code to pre-load the marker icons that are used on the maps. This will speed the loading and display of the maps. ';
    $template_tags['addon_transparentmaps_search_results'] = 'Search Results Template - Embeds a map displaying the search results on that page ';
    $template_tags['addon_transparentmaps_showmap_searchresults'] = 'Search Results Template - Embeds an individual map displaying the current listing on each search result. This tag must be placed inside the {search_result_dataset} template block on your search results template. ';
    $template_tags['addon_transparentmaps_showmap_searchresults_link'] = 'Search Results Template - Embeds an link to the individual map displaying the current listing on each search result. This tag must be placed inside the {search_result_dataset} template block on your search results template. ';
    $template_tags['addon_transparentmaps_area_searchresults'] = 'Search Results Template - Embeds an individual map displaying the listings in the area of each search result. This tag must be placed inside the {search_result_dataset} template block on your search results template. ';
    $template_tags['addon_transparentmaps_area_searchresults_link'] = 'Search Results Template - Embeds an link to the individual map displaying the listings in the area of each search result. This tag must be placed inside the {search_result_dataset} template block on your search results template. ';
    $template_tags['addon_transparentmaps_showmap'] = 'Listing Details Template - Embeds a map displaying only the current listing ';
    $template_tags['addon_transparentmaps_showmap_link'] = 'Listing Details Template - Places a link to a map for only the current listing. (Popup or inline map per configuration) ';
    $template_tags['addon_transparentmaps_area'] = 'Listing Details Template - Embeds a map displaying listings within a defined distance from the current listing. (Distance and zoom level in configuration) ';
    $template_tags['addon_transparentmaps_area_link'] = 'Listing Details Template - Places a link to a map displaying listings within a defined distance from the current listing. Popup or inline map per configuration) ';
    $template_tags['addon_transparentmaps_user'] = 'View Agent Template - Embeds a map displaying all of the current agent\'s listings. ';
    $template_tags['addon_transparentmaps_user_link'] = 'View Agent Template - Places a link to a map displaying all of the current agent\'s listings. ';
    $template_tags['addon_transparentmaps_showall'] = 'Embeds a map that shows all listings in the database. Caution should be used, if you have a large number of listings it will take a very long time to obtain and display all of the listings and the java map applet will require a lot of system resources. This should only be used for sites with around 200 or less listings. ';
    $template_tags['addon_transparentmaps_showall_link'] = 'Places a link to a map that shows all listings in the database. Caution should be used, if you have a large number of listings it will take a very long time to obtain and display all of the listings and the java map applet will require a lot of system resources. This should only be used for sites with around 200 or less listings. ';
    $action_urls['addon_transparentmaps_showmap'] = 'Opens a listing map, needs a listingID passed as a GET valiable as well.';
    return [$template_tags, $action_urls, $doc_url];
}

function transparentmaps_uninstall_tables()
{
    global $conn, $config;
    global $conn, $config;
    require_once($config['basepath'] . '/include/misc.inc.php');
    $misc = new Misc();

    $sql_uninstall[] = 'DROP TABLE ' . $config['table_prefix_no_lang'] . 'addons_transparentmaps';
    $sql_uninstall[] = 'DROP TABLE transparentmaps_license';
    $sql_uninstall[] = "DELETE FROM  " . $config['table_prefix_no_lang'] . "addons 
						WHERE addons_name ='transparentmaps'";

    foreach ($sql_uninstall as $elementContents) {
        $recordSet = $conn->Execute($elementContents);
        if (!$recordSet) {
            echo "<strong><span style=\"red\">ERROR - $elementContents</span></strong><br />";
            return false;
        } else {
            return true;
        }
    }
}

function transparentmaps_run_action_user_template()
{
    switch ($_GET['action']) {
        case 'addon_transparentmaps_showmap':
            $data = transparentmaps_showmap($_GET['listingID']);
            break;
        case 'addon_transparentmaps_area':
            $data = transparentmaps_area($_GET['listingID']);
            break;
        case 'addon_transparentmaps_area_searchresults':
            $data = transparentmaps_area_searchresults($_GET['listingID']);
            break;
        case 'addon_transparentmaps_showmap_searchresults':
            $data = transparentmaps_showmap_searchresults($_GET['listingID']);
            break;
        case 'addon_transparentmaps_user':
            $data = transparentmaps_user($_GET['userID']);
            break;
        case 'addon_transparentmaps_showall':
            $pclass = '';
            if (isset($_GET['pclass'])) {
                //the GET var is an array so treat as such
                $pclass = intval($_GET['pclass'][0]);
            }
            $data = transparentmaps_showall($pclass);
            break;
        default:
            $data = '';
            break;
    }
    return $data;
}

function transparentmaps_run_action_ajax()
{
    ini_set('max_execution_time', 0);

    switch ($_GET['action']) {
        case 'addon_transparentmaps_geocode_all':
            $data = transparentmaps_geocode_all();
            break;
        case 'addon_transparentmaps_geocode_all_refresh':
            $data = transparentmaps_geocode_all($refresh = 'yes');
            break;
        default:
            $data = '';
            break;
    }

    return $data;
}


function transparentmaps_run_action_admin_template()
{
    ini_set('max_execution_time', 0);
    $data = show_header();
    switch ($_GET['action']) {
        case 'addon_transparentmaps_admin':
            $data .= transparentmaps_display_admin_page();
            break;
        case 'addon_transparentmaps_geocode_all':
            $data = transparentmaps_geocode_all();
            break;
        case 'addon_transparentmaps_geocode_all_refresh':
            $data = transparentmaps_geocode_all($refresh = 'yes');
            break;
        default:
            $data = '';
            break;
    }
    $data .= show_footer();
    return $data;
}

function show_header()
{
    global $config, $loadjs, $jscript;

    if (defined('TransparentMAPS_REGISTEREDNAME')) {
        $rname = TransparentMAPS_REGISTEREDNAME;
    }
    if (defined('TransparentMAPS_REGCOMPANY')) {
        $rcomp = TransparentMAPS_REGCOMPANY;
    }
    if (defined('TransparentMAPS_PRODUCTNAME')) {
        $prodnam = TransparentMAPS_PRODUCTNAME;
    }
    if (defined('TransparentMAPS_REGDATE')) {
        $regdate = TransparentMAPS_REGDATE;
    }
    if (defined('TransparentMAPS_KEYNUM')) {
        $key_num = TransparentMAPS_KEYNUM;
    }

    $jscript .= '<style type="text/css">

	#lic_div {
		padding: 4px;
		margin-bottom: 10px;
		background: #F4F7FF;
		background: -moz-linear-gradient(top, #F4F7FF 0%, #D2D8E0 40%, #C0CEC7 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F4F7FF), color-stop(40%,#D2D8E0), color-stop(100%,#C0CEC7));
		background: -webkit-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: -o-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: -ms-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: linear-gradient(to bottom, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f4f7ff", endColorstr="#c0cec7",GradientType=0 );
		-webkit-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
		-moz-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
		box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75)
	}

	#lic_div div {
		margin-top: 0em;
		display: inline-block;
		line-height: 24px;
	}

	#lic_table {
		width: 100%;
		text-align: right;
		padding: 4px 0px;
		margin: 0px
		background: #F4F7FF;
		background: -moz-linear-gradient(top, #F4F7FF 0%, #D2D8E0 40%, #C0CEC7 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F4F7FF), color-stop(40%,#D2D8E0), color-stop(100%,#C0CEC7));
		background: -webkit-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: -o-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: -ms-linear-gradient(top, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		background: linear-gradient(to bottom, #F4F7FF 0%,#D2D8E0 40%,#C0CEC7 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f4f7ff", endColorstr="#c0cec7",GradientType=0 );
		-webkit-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
		-moz-box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
		box-shadow: 1px 1px 2px rgba(1, 0, 0, 0.75);
	}

	#lic_table td a {
		border: 1px solid #999;
		margin: 0px 1px;
		padding: 4px 6px;
		background: #f2f2f2;
		background: -moz-linear-gradient(top, #f2f2f2 0%, #fcfcfc 41%, #e0e0e0 100%);
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f2f2f2), color-stop(41%,#fcfcfc), color-stop(100%,#e0e0e0));
		background: -webkit-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
		background: -o-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
		background: -ms-linear-gradient(top, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
		background: linear-gradient(to bottom, #f2f2f2 0%,#fcfcfc 41%,#e0e0e0 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#f2f2f2", endColorstr="#e0e0e0",GradientType=0 );
	}

	#lic_table td img {
		vertical-align: bottom;
		width: 16px;
		height: 16px;
	}

	</style>';


    $display = '
    <div class="card card-frame mb-4">
      <div class="card-body py-2">
					<div class="mb-2">
						<img src="' . $config['baseurl'] . '/addons/transparentmaps/images/transparentmaps.gif" height="24" /> TransparentMAPS <span style="font-size: 8pt;">v' . current_version() . '</span>
					</div>
    ';

    return $display;
}

function show_footer()
{
    $display = ' <div class="copyright text-center text-sm text-muted text-lg-start">
				TransparentMaps &copy;<span class="copyright_year"></span> Made with
                  <i class="fa fa-heart"></i> by Ryan Bonham.
			</div>
            </div></div>';

    return $display;
}

function transparentmaps_run_template_user_fields($tag = '')
{
    global $current_ID;
    switch ($tag) {
        case 'addon_transparentmaps_loadgoogleapis':
            $data = transparentmaps_load_google_api();
            break;
        case 'addon_transparentmaps_preloadicons':
            $data = transparentmaps_preloadicons();
            break;
        case 'addon_transparentmaps_search_results':
            $data = transparentmaps_search_results();
            break;
        case 'addon_transparentmaps_showmap_searchresults':
            $data = transparentmaps_showmap_searchresults($current_ID);
            break;
        case 'addon_transparentmaps_showmap_searchresults_link':
            $data = transparentmaps_showmap_searchresults_link($current_ID);
            break;
        case 'addon_transparentmaps_showmap_searchresults_link_raw':
            $data = transparentmaps_showmap_searchresults_link($current_ID, true);
            break;
        case 'addon_transparentmaps_showmap':
            $data = transparentmaps_showmap($_GET['listingID']);
            break;
        case 'addon_transparentmaps_showmap_link':
            $data = transparentmaps_showmap_link($_GET['listingID']);
            break;
        case 'addon_transparentmaps_showmap_link_raw':
            $data = transparentmaps_showmap_link($_GET['listingID'], true);
            break;
        case 'addon_transparentmaps_area_searchresults':
            $data = transparentmaps_area_searchresults($current_ID);
            break;
        case 'addon_transparentmaps_area_searchresults_link':
            $data = transparentmaps_area_searchresults_link($current_ID);
            break;
        case 'addon_transparentmaps_area_searchresults_link_raw':
            $data = transparentmaps_area_searchresults_link($current_ID, true);
            break;
        case 'addon_transparentmaps_area':
            $data = transparentmaps_area($_GET['listingID']);
            break;
        case 'addon_transparentmaps_area_link':
            $data = transparentmaps_area_link($_GET['listingID']);
            break;
        case 'addon_transparentmaps_area_link_raw':
            $data = transparentmaps_area_link($_GET['listingID'], true);
            break;
        case 'addon_transparentmaps_user':
            $data = transparentmaps_user($_GET['user']);
            break;
        case 'addon_transparentmaps_user_link':
            $data = transparentmaps_user_link($_GET['user']);
            break;
        case 'addon_transparentmaps_user_link_raw':
            $data = transparentmaps_user_link($_GET['user'], true);
            break;
        case 'addon_transparentmaps_showall':
            $data = transparentmaps_showall($pclass = '');
            break;
        case 'addon_transparentmaps_showall_link':
            $data = transparentmaps_showall_link();
            break;
        case 'addon_transparentmaps_showall_link_raw':
            $data = transparentmaps_showall_link(true);
            break;
        case (preg_match('/addon_transparentmaps_showall_([0-9]{1,2})/', $tag) ? $tag : false):
            $pclass = intval(str_replace('addon_transparentmaps_showall_', '', $tag));
            //$data="HERE 1 - ".$pclass;
            $data = transparentmaps_showall($pclass);
            break;
        case (preg_match('/addon_transparentmaps_showall_link_([0-9]{1,2})/', $tag) ? $tag : false):
            $pclass = intval(str_replace('addon_transparentmaps_showall_link_', '', $tag));
            //$data="HERE 2 - ".$pclass;
            $data = transparentmaps_showall_link_pclass($pclass, false);
            break;
        case (preg_match('/addon_transparentmaps_showall_link_raw_([0-9]{1,2})/', $tag) ? $tag : false):
            $pclass = intval(str_replace('addon_transparentmaps_showall_link_raw_', '', $tag));
            //$data="HERE 3 - ".$pclass;
            $data = transparentmaps_showall_link_pclass($pclass, true);
            break;
        default:
            $data = '';
            break;
    }
    return $data;
}

/**********************************
 *    Addon Specific Functions    *
 **********************************/
//////////////////////////////////////////////////////////////////
/*                      ADMIN PAGE                              */
//////////////////////////////////////////////////////////////////
function transparentmaps_display_admin_page()
{
    global $conn, $lang, $config, $jscript, $misc;
    require_once('../include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $security = $login->loginCheck('edit_site_config', true);
    $display = '';
    $panel_id = '';

    if ($security === true) {
        // Listing Template Field Names for Field Selection
        $sql = "SELECT listingsformelements_field_name, listingsformelements_field_caption 
				FROM " . $config['table_prefix'] . "listingsformelements";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $listing_field_name_options[''] = '';
        while (!$recordSet->EOF) {
            $field_name = $recordSet->fields('listingsformelements_field_name');
            $listing_field_name_options[$field_name] = $field_name . ' (' . $recordSet->fields('listingsformelements_field_caption') . ')';
            $recordSet->MoveNext();
        }
        // Generate GuideString
        $guidestring = '';
        foreach ($_GET as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $vitem) {
                    $guidestring .= '&' . urlencode("$k") . '[]=' . urlencode("$vitem");
                }
            } else {
                $guidestring .= '&' . urlencode("$k") . '=' . urlencode("$v");
            }
        }

        // Save any Post Data
        if (isset($_POST['addons_transparentmaps_apikey'])) {
            //get the hash we stored on the previous page's form
            $panel_id = $_POST['panel_id'];

            // Update ControlPanel
            $sql = 'UPDATE ' . $config['table_prefix_no_lang'] . 'addons_transparentmaps SET ';
            $sql_part = '';
            //print_r($_POST);
            foreach ($_POST as $field => $value) {
                if (strpos($field, 'addons_') !== 0) {
                    continue;
                }
                if (is_array($value)) {
                    $value2 = '';
                    foreach ($value as $f) {
                        if ($value2 == '') {
                            $value2 = "$f";
                        } else {
                            $value2 .= ",$f";
                        }
                    }
                    if ($sql_part == '') {
                        $sql_part = "$field = '$value2'";
                    } else {
                        $sql_part .= " , $field = '$value2'";
                    }
                } else {
                    $value = $misc->make_db_safe($value);
                    if ($sql_part == '') {
                        $sql_part = "$field = $value";
                    } else {
                        $sql_part .= " , $field = $value";
                    }
                }
            }
            $sql .= $sql_part;
            $recordSet = $conn->Execute($sql);
            if (!$recordSet) {
                $misc->log_error($sql);
            }
            //set the page hash so we go back to the original tab, and set the rivet to show "configuration Saved"
            $display .= '<script type="text/javascript">
							var hash = "#' . $panel_id . '";
							location.hash = hash;
							status_msg("' . $lang['configuration_saved'] . '",focus);
						</script>';

            //$display .= '<br /><b>' . $lang['configuration_saved'] . '</b><br />';
        }
        $sql = 'SELECT * from ' . $config["table_prefix_no_lang"] . 'addons_transparentmaps';
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        // Include the Form Generation Class
        include(dirname(__FILE__) . '/form_generation.inc.php');
        // Default Options
        $yes_no[0] = 'No';
        $yes_no[1] = 'Yes';
        $priority[0] = 'Disabled';
        $priority[1] = 'First';
        $priority[2] = 'Second';
        $priority[3] = 'Third';
        $number_format[1] = '1,000.00';
        $number_format[2] = '1.000,00';
        $number_format[3] = '1 000.00';
        $number_format[4] = '1 000,00';
        $number_format[5] = '1\'000,00';
        $number_format[6] = '1-000 00';
        $caption_opts['value'] = 'value';
        $caption_opts['caption'] = 'caption';
        $caption_opts['both'] = 'both';
        $zoom_level['0'] = '0';
        $zoom_level['1'] = '1';
        $zoom_level['2'] = '2';
        $zoom_level['3'] = '3';
        $zoom_level['4'] = '4';
        $zoom_level['5'] = '5';
        $zoom_level['6'] = '6';
        $zoom_level['7'] = '7';
        $zoom_level['8'] = '8';
        $zoom_level['9'] = '9';
        $zoom_level['10'] = '10';
        $zoom_level['11'] = '11';
        $zoom_level['12'] = '12';
        $zoom_level['13'] = '13';
        $zoom_level['14'] = '14';
        $zoom_level['15'] = '15';
        $zoom_level['16'] = '16';
        $zoom_level['17'] = '17';
        $zoom_level['17'] = '18';
        $zoom_level['17'] = '19';
        $autozoom['az'] = 'Auto Zoom';
        $autozoom['1'] = 'Zoom Level 1';
        $autozoom['2'] = 'Zoom Level 2';
        $autozoom['3'] = 'Zoom Level 3';
        $autozoom['4'] = 'Zoom Level 4';
        $autozoom['5'] = 'Zoom Level 5';
        $autozoom['6'] = 'Zoom Level 6';
        $autozoom['7'] = 'Zoom Level 7';
        $autozoom['8'] = 'Zoom Level 8';
        $autozoom['9'] = 'Zoom Level 9';
        $autozoom['10'] = 'Zoom Level 10';
        $autozoom['11'] = 'Zoom Level 11';
        $autozoom['12'] = 'Zoom Level 12';
        $autozoom['13'] = 'Zoom Level 13';
        $autozoom['14'] = 'Zoom Level 14';
        $autozoom['15'] = 'Zoom Level 15';
        $autozoom['16'] = 'Zoom Level 16';
        $autozoom['17'] = 'Zoom Level 17';
        $autozoom['18'] = 'Zoom Level 18';
        $autozoom['19'] = 'Zoom Level 19';
        $autozoom['az1'] = 'Auto Zoom minus 1 Level';
        $autozoom['az2'] = 'Auto Zoom minus 2 Levels';
        $autozoom['az3'] = 'Auto Zoom minus 3 Levels';
        $autozoom['az4'] = 'Auto Zoom minus 4 Levels';
        $autozoom['az5'] = 'Auto Zoom minus 5 Levels';
        $autozoom['az6'] = 'Auto Zoom minus 6 Levels';
        $autozoom['az7'] = 'Auto Zoom minus 7 Levels';
        $autozoom['az8'] = 'Auto Zoom minus 8 Levels';
        $autozoom['az9'] = 'Auto Zoom minus 9 Levels';
        $autozoom['az10'] = 'Auto Zoom minus 10 Levels';
        $autozoom['az11'] = 'Auto Zoom minus 11 Levels';
        $autozoom['az12'] = 'Auto Zoom minus 12 Levels';
        $autozoom['az13'] = 'Auto Zoom minus 13 Levels';
        $autozoom['az14'] = 'Auto Zoom minus 14 Levels';
        $autozoom['az15'] = 'Auto Zoom minus 15 Levels';
        $autozoom['az16'] = 'Auto Zoom minus 16 Levels';
        $autozoom['az17'] = 'Auto Zoom minus 17 Levels';
        $autozoom['az18'] = 'Auto Zoom minus 18 Levels';
        $default_maptype['G_NORMAL_MAP'] = 'Normal Map';
        $default_maptype['G_SATELLITE_MAP'] = 'Satellite Map';
        $default_maptype['G_HYBRID_MAP'] = 'Hybrid Map';
        $default_maptype['G_PHYSICAL_MAP'] = 'Terrain Map';
        $default_maptype['G_SATELLITE_3D_MAP'] = '3D Satellite View';
        $pano_types['NONE'] = 'Do Not Display';
        $pano_types['INFOWIN'] = 'InfoWindow';
        $pano_types['EXTDIV'] = 'Custom Div';

        // Get List of Available Geocoders
        $geocoders = [];

        if ($handle = opendir($config['basepath'] . '/addons/transparentmaps/geocoders')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && $file != "CVS" && $file != ".svn") {
                    if (!is_dir($config['basepath'] . '/addons/transparentmaps/geocoders/' . $file)) {
                        $geocoders[substr($file, 0, -8)] = substr($file, 0, -8);
                    }
                }
            }
            closedir($handle);
        }

        $formGen = new formGeneration();
        $display .= $formGen->startform('index.php?' . $guidestring, 'post', 'multipart/form-data', 'tmaps_config');
        //Start tabbed page

        $display .= $formGen->createformitem('hidden', 'panel_id', $panel_id, false, '20', '', '', '', '', '', '');
        $display .= '	
        <div class="nav-wrapper position-relative end-0">
        <ul
            class="nav nav-pills nav-fill p-1"
            role="tablist"
        >
        
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1 active"
                    id="transparentmaps_CONFIG_geocoderapi_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_geocoderapi_pane"
                    aria-controls="transparentmaps_CONFIG_geocoderapi_pane"
                    aria-selected="true"
                    >{lang_tmaps_config_geocoderapi}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_GeneralMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_GeneralMap_pane"
                    aria-controls="transparentmaps_CONFIG_GeneralMap_pane"
                    aria-selected="false"
                    >{lang_tmaps_general_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_InfoWindow_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_InfoWindow_pane"
                    aria-controls="transparentmaps_CONFIG_InfoWindow_pane"
                    aria-selected="false"
                    >{lang_tmaps_infowindow_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_SearchResultMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_SearchResultMap_pane"
                    aria-controls="transparentmaps_CONFIG_SearchResultMap_pane"
                    aria-selected="false"
                    >{lang_tmaps_search_results_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_ListingMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_ListingMap_pane"
                    aria-controls="transparentmaps_CONFIG_ListingMap_pane"
                    aria-selected="false"
                    >{lang_tmaps_listing_map_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_AreaMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_AreaMap_pane"
                    aria-controls="transparentmaps_CONFIG_AreaMap_pane"
                    aria-selected="false"
                    >{lang_tmaps_area_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_UserMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_UserMap_pane"
                    aria-controls="transparentmaps_CONFIG_UserMap_pane"
                    aria-selected="false"
                    >{lang_tmaps_user_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_ShowAllMap_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_ShowAllMap_pane"
                    aria-controls="transparentmaps_CONFIG_ShowAllMap_pane"
                    aria-selected="false"
                    >{tmaps_showall_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_ListingMapSearchResult_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_ListingMapSearchResult_pane"
                    aria-controls="transparentmaps_CONFIG_ListingMapSearchResult_pane"
                    aria-selected="false"
                    >{lang_tmaps_listingmapsearchresult_config}</a>
                </li>
                <li class="nav-item">
                    <a
                    class="nav-link mb-0 px-0 py-1"
                    id="transparentmaps_CONFIG_AreaMapSearchResult_tab"
                    data-bs-toggle="tab"
                    role="tab"
                    data-bs-target="#transparentmaps_CONFIG_AreaMapSearchResult_pane"
                    aria-controls="transparentmaps_CONFIG_AreaMapSearchResult_pane"
                    aria-selected="false"
                    >{lang_tmaps_general_config}</a>
                </li>
            </ul>
        </div>';



        //Geocoder Configuration
        $display .= '<div class="tab-content">
        <div role="tabpanel" id="transparentmaps_CONFIG_geocoderapi_pane" class="tab-pane show active" aria-labelledby="transparentmaps_CONFIG_geocoderapi_tab">
    
            <div class="row align-items-end mb-2 mt-4">
                <div class="col-12 col-md-4">
                    <div class="input-group input-group-static mb-2">
                        <label for="addons_transparentmaps_apikey">{lang_tmaps_api_key}</label>
                        ' . $formGen->createformitem('text', 'addons_transparentmaps_apikey', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_apikey')), false, 20, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_apikey'))) . '
                    </div>
                </div>
                <div class="col text-xs">{lang_tmaps_api_key_desc}</div>
            </div>
    
            <div class="row align-items-end mb-2">
                <div class="col-12 col-md-4">
                    <div class="input-group input-group-static">
                        <label for="addons_transparentmaps_google_geocoder_priority" class="ms-0">{lang_tmaps_google_geocoder_priority}</label>
                        ' . $formGen->createformitem('select', 'addons_transparentmaps_google_geocoder_priority', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_google_geocoder_priority')), false, 20, '', '', '', '', $priority, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_google_geocoder_priority'))) . '
                    </div>
                </div>
                <div class="col text-sm">{lang_tmaps_geocoder_priority_desc}</div>
            </div>
    
    
            <div class="row align-items-end mb-2">
                <div class="col-12 col-md-4">
                    <div class="input-group input-group-static mb-2">
                        <label for="addons_transparentmaps_otherkey">{lang_tmaps_other_key}</label>
                        ' . $formGen->createformitem('text', 'addons_transparentmaps_otherkey', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_otherkey')), false, 20, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_otherkey'))) . '
                    </div>
                </div>
                <div class="col text-xs">{lang_tmaps_other_key_desc}</div>
            </div>
    
            <div class="row align-items-end mb-2">
                <div class="col-12 col-md-4">
                    <div class="input-group input-group-static">
                        <label for="addons_transparentmaps_other_geocoder_priority" class="ms-0">{lang_tmaps_other_geocoder_priority}</label>
                        ' . $formGen->createformitem('select', 'addons_transparentmaps_other_geocoder_priority', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_other_geocoder_priority')), false, 20, '', '', '', '', $priority, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_other_geocoder_priority'))) . '
                    </div>
                </div>
                <div class="col text-sm">{lang_tmaps_geocoder_priority_desc}</div>
            </div>
    
            <div class="row align-items-end mb-2">
                <div class="col-12 col-md-4">
                    <div class="input-group input-group-static mb-2">
                        <label for="addons_transparentmaps_default_country">{lang_tmaps_default_country}</label>
                        ' . $formGen->createformitem('text', 'addons_transparentmaps_default_country', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_country')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_country'))) . '
                    </div>
                </div>
                <div class="col text-xs">{lang_tmaps_default_country_desc}</div>
            </div>
    
        </div>';

        //Start General Map Configuration

        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_GeneralMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_GeneralMap_tab">


        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_use_customurl" class="ms-0">{lang_tmaps_use_customurl}</label>
                    ' . $formGen->createformitem('select', 'addons_transparentmaps_use_customurl', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_use_customurl')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_use_customurl'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_use_customurl_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_customurl">{lang_tmaps_customurl}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_customurl', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_customurl')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_customurl'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_customurl_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_default_lat">{lang_tmaps_default_lat}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_default_lat', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_lat')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_lat'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_default_lat_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_default_long">{lang_tmaps_default_long}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_default_long', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_long')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_long'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_default_long_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_default_zoom">{lang_tmaps_default_zoom}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_default_zoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_zoom')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_default_zoom'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_default_zoom_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_scroll_wheel_zoom" class="ms-0">{lang_tmaps_scroll_wheel_zoom}</label>
                    ' . $formGen->createformitem('select', 'addons_transparentmaps_scroll_wheel_zoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_scroll_wheel_zoom')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_scroll_wheel_zoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scroll_wheel_zoom_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_enable_dragging" class="ms-0">{lang_tmaps_enable_dragging}</label>
                    ' . $formGen->createformitem('select', 'addons_transparentmaps_enable_dragging', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_enable_dragging')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_enable_dragging'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_enable_dragging_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_icon_width">{lang_tmaps_icon_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_icon_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_icon_width')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_icon_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_icon_width_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_icon_height">{lang_tmaps_icon_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_icon_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_icon_height')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_icon_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_icon_height_desc}</div>
        </div>
    </div>'; //End General Map Config

        //Tab 3
        //Start Info Window Configuration
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_InfoWindow_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_InfoWindow_tab">

    <div class="row align-items-end mb-2 mt-4">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_photo" class="ms-0">{lang_tmaps_show_photo}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_photo', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_photo')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_photo'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_photo_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_title" class="ms-0">{lang_tmaps_show_title}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_title', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_title')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_title'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_title_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_address" class="ms-0">{lang_tmaps_show_address}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_address', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_address')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_address'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_address_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_city" class="ms-0">{lang_tmaps_show_city}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_city', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_city')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_city'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_city_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_state" class="ms-0">{lang_tmaps_show_state}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_state', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_state')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_state'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_state_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_show_zip" class="ms-0">{lang_tmaps_show_zip}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_show_zip', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_zip')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_show_zip'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_show_zip_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_0">{lang_tmaps_text_0}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_0', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_0')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_0'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_0_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_1" class="ms-0">{lang_tmaps_field_1}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_1', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_1')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_1'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_1_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_1" class="ms-0">{lang_tmaps_caption_field_1}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_1', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_1')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_1'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_1_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_1">{lang_tmaps_text_1}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_1', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_1')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_1'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_1_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_2" class="ms-0">{lang_tmaps_field_2}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_2', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_2')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_2'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_2_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_2" class="ms-0">{lang_tmaps_caption_field_2}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_2', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_2')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_2'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_2_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_2">{lang_tmaps_text_2}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_2', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_2')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_2'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_2_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_3" class="ms-0">{lang_tmaps_field_3}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_3', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_3')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_3'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_3_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_3" class="ms-0">{lang_tmaps_caption_field_3}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_3', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_3')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_3'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_3_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_3">{lang_tmaps_text_3}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_3', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_3')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_3'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_3_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_4" class="ms-0">{lang_tmaps_field_4}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_4', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_4')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_4'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_4_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_4" class="ms-0">{lang_tmaps_caption_field_4}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_4', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_4')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_4'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_4_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_4">{lang_tmaps_text_4}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_4', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_4')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_4'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_4_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_5" class="ms-0">{lang_tmaps_field_5}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_5', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_5')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_5'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_5_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_5" class="ms-0">{lang_tmaps_caption_field_5}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_5', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_5')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_5'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_5_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_5">{lang_tmaps_text_5}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_5', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_5')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_5'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_5_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_field_6" class="ms-0">{lang_tmaps_field_6}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_field_6', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_6')), false, 35, '', '', '', '', $listing_field_name_options, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_field_6'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_field_6_desc}</div>
    </div>
    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_caption_field_6" class="ms-0">{lang_tmaps_caption_field_6}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_caption_field_6', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_6')), false, 4, '', '', '', '', $caption_opts, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_caption_field_6'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_caption_field_6_desc}</div>
    </div>

    <div class="row align-items-end mb-2">
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static mb-2">
                <label for="addons_transparentmaps_text_6">{lang_tmaps_text_6}</label>
                ' . $formGen->createformitem('text', 'addons_transparentmaps_text_6', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_6')), false, 30, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_text_6'))) . '
            </div>
        </div>
        <div class="col text-xs">{lang_tmaps_text_6_desc}</div>
    </div>

</div>'; //End Info Window Config

        //Tab 4
        //Start Search Result Map Configuration
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_SearchResultMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_SearchResultMap_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_search_results_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_search_results_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_search_results_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_search_results_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_height')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_search_results_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_search_results_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_search_results_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_search_results_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_search_results_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    </div>'; //END Search Result Map Configuration

        //Tab 5
        //Start Listing Map Configuration
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_ListingMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_ListingMap_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showmap_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_showmap_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showmap_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_showmap_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_height')), false, 3, '', '', '', '', '', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showmap_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showmap_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_showmap_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
        <div class="col-12 col-md-4">
            <div class="input-group input-group-static">
                <label for="addons_transparentmaps_showmap_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showmap_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showmap_autozoom'))) . '
            </div>
        </div>
        <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
    </div>'; //End Tab 5

        //Tab 6
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_AreaMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_AreaMap_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_area_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_area_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_area_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_area_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_height')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_area_distance">{lang_tmaps_distance}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_area_distance', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_distance')), false, 5, '', '', '', '', '', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_distance'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_distance_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_zoom" class="ms-0">{lang_tmaps_zoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_zoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_zoom')), false, 4, '', '', '', '', $zoom_level, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_zoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_zoom_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_allclasses" class="ms-0">{lang_tmaps_classes}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_allclasses', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_allclasses')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_allclasses'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_classes_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_area_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_area_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
        
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_area_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_area_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_area_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    
    
    </div>'; //End Tab 6

        //Tab 7
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_UserMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_UserMap_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_user_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_user_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_user_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_user_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_height')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_user_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_user_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
        
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_user_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_user_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_user_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    
    </div>'; //End Tab 7

        //Tab 8
        $display .= '		<div role="tabpanel" id="transparentmaps_CONFIG_ShowAllMap_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_ShowAllMap_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showall_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_showall_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showall_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_showall_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_height')), false, 3, '', '', '', '', '', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_showall_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_showall_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
        
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_showall_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_showall_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_showall_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    
    </div>'; //End Tab 8

        //Tab 9
        //Start Listing Map Configuration
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_ListingMapSearchResult_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_ListingMapSearchResult_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_listingmapsearchresult_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_listingmapsearchresult_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_listingmapsearchresult_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_listingmapsearchresult_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_height')), false, 3, '', '', '', '', '', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_listingmapsearchresult_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_listingmapsearchresult_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
       
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_listingmapsearchresult_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_listingmapsearchresult_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_listingmapsearchresult_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    
    </div>'; //End Tab 9


        //Tab 10
        $display .= '<div role="tabpanel" id="transparentmaps_CONFIG_AreaMapSearchResult_pane" class="tab-pane" aria-labelledby="transparentmaps_CONFIG_AreaMapSearchResult_tab">

        <div class="row align-items-end mb-2 mt-4">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_popup" class="ms-0">{lang_tmaps_popup}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_popup', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_popup')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_popup'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_popup_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_areasearchresult_width">{lang_tmaps_width}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_areasearchresult_width', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_width')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_width'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_width_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_areasearchresult_height">{lang_tmaps_height}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_areasearchresult_height', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_height')), false, 3, '', '', '', '', $number_format, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_height'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_height_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_smmapcontrol" class="ms-0">{lang_tmaps_smmapcontrol}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_smmapcontrol', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_smmapcontrol')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_smmapcontrol'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_smmapcontrol_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_scale" class="ms-0">{lang_tmaps_scale}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_scale', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_scale')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_scale'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_scale_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_areasearchresult_distance">{lang_tmaps_distance}</label>
                    ' . $formGen->createformitem('text', 'addons_transparentmaps_areasearchresult_distance', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_distance')), false, 5, '', '', '', '', '', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_distance'))) . '
                </div>
            </div>
            <div class="col text-xs">{lang_tmaps_distance_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_zoom" class="ms-0">{lang_tmaps_zoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_zoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_zoom')), false, 4, '', '', '', '', $zoom_level, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_zoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_zoom_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_allclasses" class="ms-0">{lang_tmaps_classes}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_allclasses', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_allclasses')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_allclasses'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_classes_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_mapttype[]" class="ms-0">{lang_tmaps_mapttype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_mapttype[]', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_mapttype')), true, 5, '', '', '', '', $default_maptype, explode(',', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_mapttype')))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_mapttype_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_default_maptype" class="ms-0">{lang_tmaps_default_maptype}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_default_maptype', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_default_maptype')), false, 20, '', '', '', '', $default_maptype, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_default_maptype'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_default_maptype_desc}</div>
        </div>
    
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_directions" class="ms-0">{lang_tmaps_directions}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_directions', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_directions')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_directions'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_directions_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_streetview" class="ms-0">{lang_tmaps_streetview}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_streetview', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_streetview')), false, 35, '', '', '', '', $yes_no, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_streetview'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_streetview_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_streetviewpano" class="ms-0">{lang_tmaps_streetviewpano}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_streetviewpano', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_streetviewpano')), false, 35, '', '', '', '', $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_streetviewpano'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_sreetviewpano_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static mb-2">
                    <label for="addons_transparentmaps_areasearchresult_klmoverlay">{lang_tmaps_klmoverlay}</label>
                    ' . $formGen->createformitem('textarea', 'addons_transparentmaps_areasearchresult_klmoverlay', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_klmoverlay')), false, 35, '', '', 5, 35, $pano_types, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_klmoverlay'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_klmoverlay_desc}</div>
        </div>
        <div class="row align-items-end mb-2">
            <div class="col-12 col-md-4">
                <div class="input-group input-group-static">
                    <label for="addons_transparentmaps_areasearchresult_autozoom" class="ms-0">{lang_tmaps_autozoom}</label>' . $formGen->createformitem('select', 'addons_transparentmaps_areasearchresult_autozoom', $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_autozoom')), false, 20, '', '', '', '', $autozoom, $misc->make_db_unsafe($recordSet->fields('addons_transparentmaps_areasearchresult_autozoom'))) . '
                </div>
            </div>
            <div class="col text-sm">{lang_tmaps_autozoom_desc}</div>
        </div>
    
    </div>'; //End Tab 10
        //End Tabs
        $display .= '	</div>';

        //submit
        $display .= '	<div style="margin-left: auto; margin-right: auto; width: 100px; padding: 20px;">
							' . $formGen->createformitem('submit', '', '' . $lang['tmaps_save_changes'] . '') . '	
						</div>';

        $display .= $formGen->endform();
    } else {
        $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }
    return $display;
}
//////////////////////////////////////////////////////////////////
/*                      -END ADMIN PAGE-                        */
//////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////
/*                       SEARCH RESULTS                         */
//////////////////////////////////////////////////////////////////
function transparentmaps_search_results()
{
    global $config, $lang, $conn, $jscript, $jscript_last;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_search_result';
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_search_results_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_search_results_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_search_results_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_search_results_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_search_results_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_search_results_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_search_results_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_search_results_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_search_results_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_search_results_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_search_results_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_search_results_streetviewpano");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_search_results_klmoverlay"));
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");

    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];

    $display = '';
    // Here's the query string, now clean it up
    $query_string = '';
    foreach ($_GET as $k => $v) {
        if (is_array($v)) {
            foreach ($v as $vitem) {
                $query_string .= '&' . urlencode("$k") . '[]=' . urlencode("$vitem");
            }
        } else {
            $query_string .= '&' . urlencode("$k") . '=' . urlencode("$v");
        }
    }
    // Break Query String up into $_GET variables.
    // $query_string= urldecode($query_string);
    //$query_string = substr($query_string, 5);
    $tmaps->query_string = $query_string;
    // Add the DIV for the map
    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_search_results
//////////////////////////////////////////////////////////////////
/*                      -END SEARCH RESULTS-                    */
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
/*                          SHOW MAP                            */
//////////////////////////////////////////////////////////////////
function transparentmaps_showmap($listingID)
{
    global $config, $lang, $conn, $jscript, $jscript_last;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_listingmap';
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_showmap_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_showmap_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_showmap_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_showmap_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_showmap_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_showmap_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_showmap_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_showmap_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_showmap_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_showmap_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_showmap_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_showmap_streetviewpano");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_showmap_klmoverlay"));
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $geo = geocode_listing($listingID);
    $lat_value = $geo['latitude'];
    $long_value = $geo['longitude'];
    if ($lat_value != 0 && $long_value != 0) {
        $tmaps->center_lat = $lat_value;
        $tmaps->center_long = $long_value;
        $tmaps->origional_lat_value = $lat_value;
        $tmaps->origional_long_value = $long_value;
    }
    $display = '';
    $query_string = 'listing_id=';
    $query_string .= $listingID;
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    $tmaps->query_string = $query_string;
    // Add the DIV for the map
    //$jscript_last .= $tmaps->call_google_api();
    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_showmap
//////////////////////////////////////////////////////////////////
/*                       -END SHOW MAP-                         */
//////////////////////////////////////////////////////////////////

function transparentmaps_showmap_searchresults($listingID)
{
    global $config, $lang, $conn, $jscript, $jscript_last;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_listingmap' . $listingID;
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_listingmapsearchresult_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_listingmapsearchresult_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_streetviewpano");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_listingmapsearchresult_klmoverlay"));
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $display = '';
    $query_string = 'listing_id=';
    $query_string .= $listingID;
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    $tmaps->query_string = $query_string;
    // Add the DIV for the map
    //$jscript_last .= $tmaps->call_google_api();
    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_showmap

function transparentmaps_load_google_api()
{
    global $config, $jscript, $conn;
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    $misc = new misc();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $tmaps = new transparentmaps_googleapi();
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $tmaps->js_folderURL =  $config['baseurl'] . '/addons/transparentmaps/JS';
    $tmaps->css_folderURL =  $config['baseurl'] . '/addons/transparentmaps/CSS';
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    return $tmaps->call_google_api();
}

function transparentmaps_preloadicons()
{
    global $config, $jscript, $conn;
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    return $tmaps->preload_icons();
}

//////////////////////////////////////////////////////////////////
/*                          AREA MAP                            */
//////////////////////////////////////////////////////////////////
function transparentmaps_area($listingID)
{
    global $config, $lang, $conn, $jscript, $jscript_last, $api;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    require_once($config['basepath'] . '/include/listing.inc.php');
    $listing_pages = new listing_pages();
    $misc = new misc();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_areamap';
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_area_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_area_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_area_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_area_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_area_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_area_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_area_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_area_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_area_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_area_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_area_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_area_streetviewpano");
    $tmaps->distance = $recordSet->fields("addons_transparentmaps_area_distance");
    //$tmaps->distance=2;
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_area_zoom");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_area_klmoverlay"));
    $show_all_classes = $recordSet->fields('addons_transparentmaps_area_allclasses');
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    //$tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");

    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $display = '';
    //$mapttype = $recordSet->fields("addons_transparentmaps_area_mapttype");
    $geo = geocode_listing($listingID);
    $lat_value = $geo['latitude'];
    $long_value = $geo['longitude'];
    if ($lat_value != 0 && $long_value != 0) {
        $tmaps->center_lat = $lat_value;
        $tmaps->center_long = $long_value;
        $tmaps->origional_lat_value = $lat_value;
        $tmaps->origional_long_value = $long_value;
    }

    // BUILD THE QUERY STRING WITH OUR LAT/LONG DATA
    $query_string = '&latlong_dist_lat=' . $lat_value . '&latlong_dist_long=' . $long_value . '&latlong_dist_dist=' . $tmaps->distance . '';
    $query_string .= '&latlong_dist_lat_orig=' . $lat_value . '&latlong_dist_long_orig=' . $long_value;
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    if ($show_all_classes != '1') {
        $api_result = $api->load_local_api('listing__read', ['listing_id' => intval($listingID), 'fields' => ['listingsdb_pclass_id']]);
        if ($api_result['error']) {
            //If an error occurs die and show the error msg;
            die($api_result['error_msg']);
        }
        $class_id = $api_result['listing']['listingsdb_pclass_id'];
        $query_string .= '&pclass[]=' . $class_id;
        $tmaps->pclass = [$class_id];
    }
    $tmaps->query_string = $query_string;
    // Add the DIV for the map

    $jscript_last .= $tmaps->create_google_map(true);
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_area
//////////////////////////////////////////////////////////////////
/*                       -END AREA MAP-                         */
//////////////////////////////////////////////////////////////////

function transparentmaps_area_searchresults($listingID)
{
    global $config, $lang, $conn, $jscript, $jscript_last, $api;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    require_once($config['basepath'] . '/include/listing.inc.php');
    $listing_pages = new listing_pages();
    $misc = new misc();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_areamap' . $listingID;
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_areasearchresult_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_areasearchresult_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_areasearchresult_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_areasearchresult_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_areasearchresult_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_areasearchresult_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_areasearchresult_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_areasearchresult_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_areasearchresult_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_areasearchresult_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_areasearchresult_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_areasearchresult_streetviewpano");
    $tmaps->distance = $recordSet->fields("addons_transparentmaps_areasearchresult_distance");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_areasearchresult_zoom");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_areasearchresult_klmoverlay"));
    $show_all_classes = $recordSet->fields('addons_transparentmaps_areasearchresult_allclasses');
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $display = '';
    //$mapttype = $recordSet->fields("addons_transparentmaps_area_mapttype");
    $geo = geocode_listing($listingID);
    $lat_value = $geo['latitude'];
    $long_value = $geo['longitude'];
    if ($lat_value != 0 && $long_value != 0) {
        $tmaps->center_lat = $lat_value;
        $tmaps->center_long = $long_value;
        $tmaps->origional_lat_value = $lat_value;
        $tmaps->origional_long_value = $long_value;
    }

    // BUILD THE QUERY STRING WITH OUR LAT/LONG DATA
    $query_string = '&latlong_dist_lat=' . $lat_value . '&latlong_dist_long=' . $long_value . '&latlong_dist_dist=' . $tmaps->distance . '';
    $query_string .= '&latlong_dist_lat_orig=' . $lat_value . '&latlong_dist_long_orig=' . $long_value;
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    if ($show_all_classes != '1') {
        $api_result = $api->load_local_api('listing__read', ['listing_id' => intval($listingID), 'fields' => ['listingsdb_pclass_id']]);
        if ($api_result['error']) {
            //If an error occurs die and show the error msg;
            die($api_result['error_msg']);
        }
        $class_id = $api_result['listing']['listingsdb_pclass_id'];
        $query_string .= '&pclass[]=' . $class_id;
        $tmaps->pclass = [$class_id];
    }
    $tmaps->query_string = $query_string;
    // Add the DIV for the map

    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_area

//////////////////////////////////////////////////////////////////
/*                      USER LISTINGS MAP                       */
//////////////////////////////////////////////////////////////////
function transparentmaps_user($userID)
{
    global $config, $lang, $conn, $jscript, $jscript_last;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_usermap';
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_user_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_user_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_user_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_user_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_user_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_user_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_user_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_user_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_user_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_user_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_user_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_user_streetviewpano");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_user_klmoverlay"));
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $display = '';
    $query_string = 'user_ID=' . $userID;
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    // Add the javascript for the map and some other functions
    $tmaps->query_string = $query_string;
    // Add the DIV for the map

    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_user
//////////////////////////////////////////////////////////////////
/*                    -END USER LISTINGS MAP-                   */
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
/*                          SHOW ALL                            */
//////////////////////////////////////////////////////////////////
function transparentmaps_showall($pclass)
{
    global $config, $lang, $conn, $jscript, $jscript_last;
    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    require_once($config['basepath'] . '/addons/transparentmaps/tmaps.inc.php');
    $tmaps = new transparentmaps_googleapi();
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Set paths for Map API
    $tmaps->dataXmlFolder = $config['baseurl'] . '/addons/transparentmaps';
    $tmaps->iconfolderURL = $config['baseurl'] . '/addons/transparentmaps/icons';
    $tmaps->iconfolder = $config['basepath'] . '/addons/transparentmaps/icons';
    //Set Map Name
    $tmaps->map_name = 'tmaps_showall';
    $tmaps->autobounds = $recordSet->fields('addons_transparentmaps_showall_autozoom');
    $tmaps->apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $usepopup = $recordSet->fields("addons_transparentmaps_showall_popup");
    $tmaps->map_width = $recordSet->fields("addons_transparentmaps_showall_width");
    $tmaps->map_height = $recordSet->fields("addons_transparentmaps_showall_height");
    $tmaps->show_small_map_controls = $recordSet->fields("addons_transparentmaps_showall_smmapcontrol");
    $tmaps->show_map_scale = $recordSet->fields("addons_transparentmaps_showall_scale");
    $tmaps->default_map_type = $recordSet->fields("addons_transparentmaps_showall_default_maptype");
    $tmaps->show_map_type_controls = explode(',', $recordSet->fields("addons_transparentmaps_showall_mapttype"));
    $tmaps->show_local_search = $recordSet->fields("addons_transparentmaps_showall_localsearch");
    $tmaps->show_directions = $recordSet->fields("addons_transparentmaps_showall_directions");
    $tmaps->show_streetview = $recordSet->fields("addons_transparentmaps_showall_streetview");
    $tmaps->show_pano = $recordSet->fields("addons_transparentmaps_showall_streetviewpano");
    $tmaps->kml_overlay_urls = explode(',', $recordSet->fields("addons_transparentmaps_showall_klmoverlay"));
    //Load Map Defaults
    $tmaps->center_lat = $recordSet->fields("addons_transparentmaps_default_lat");
    $tmaps->center_long = $recordSet->fields("addons_transparentmaps_default_long");
    $tmaps->default_zoom = $recordSet->fields("addons_transparentmaps_default_zoom");
    $tmaps->ScrollWheelZoom = $recordSet->fields("addons_transparentmaps_scroll_wheel_zoom");
    $tmaps->enableContinuousZoom = $recordSet->fields("addons_transparentmaps_continuous_zoom");
    $tmaps->enableDragging = $recordSet->fields("addons_transparentmaps_enable_dragging");
    $tmaps->icon_width = $recordSet->fields("addons_transparentmaps_icon_width");
    $tmaps->icon_height = $recordSet->fields("addons_transparentmaps_icon_height");
    $tmaps->shadow_width = $recordSet->fields("addons_transparentmaps_shadow_width");
    $tmaps->shadow_height = $recordSet->fields("addons_transparentmaps_shadow_height");
    //Load Langs
    $tmaps->lang_unable_to_locate_message = $lang['tmaps_unable_to_locate'];
    $tmaps->lang_avoid_highways = $lang['avoid_highways'];
    $tmaps->lang_walk = $lang['walk'];
    $tmaps->lang_to_here = $lang['to_here'];
    $tmaps->lang_from_here = $lang['from_here'];
    $tmaps->lang_directions = $lang['get_directions'];
    $tmaps->lang_start_address = $lang['start_address'];
    $tmaps->lang_end_address = $lang['start_address'];
    $tmaps->lang_get_directions = $lang['get_directions'];
    $display = '';
    $query_string = 'latlong_dist_lat=0&latlong_dist_long=0&latlong_dist_dist=25000';
    if ($usepopup == 1) {
        $query_string .= '&printer_friendly=yes';
    }
    if ($pclass != '') {
        $pclass = intval($pclass);
        $query_string = $query_string . '&pclass[]=' . $pclass;
    }

    // Add the javascript for the map and some other functions
    $tmaps->query_string = $query_string;
    // Add the DIV for the map

    $jscript_last .= $tmaps->create_google_map();
    $display = $tmaps->create_google_map_div();
    return $display;
} // end function transparentmaps_showall
//////////////////////////////////////////////////////////////////
/*                       -END SHOW ALL-                         */
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
/*                        -AREA MAP LINK-                       */
//////////////////////////////////////////////////////////////////
function transparentmaps_area_link($listingID, $raw_url = false)
{
    global $config, $lang, $conn, $current_ID, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_area_popup");
    $width = $recordSet->fields("addons_transparentmaps_area_width");
    $height = $recordSet->fields("addons_transparentmaps_area_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    // If a listing ID isn't set.. let them know something is wrong
    if ($listingID == '') {
        $display = 'No Listing ID';
        return $display;
    }
    if ($raw_url) {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_area&printer_friendly=yes&listingID=' . $listingID;
        } else {
            $display = 'index.php?action=addon_transparentmaps_area&listingID=' . $listingID;
        }
    } else {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_area&printer_friendly=yes&listingID=' . $listingID . '" onclick="window.open(\'index.php?action=addon_transparentmaps_area&printer_friendly=yes&listingID=' . $listingID . '\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_area_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_area&listingID=' . $listingID . '">' . $lang['tmaps_area_link'] . '</a>';
        }
    }

    return $display;
} // end function transparentmaps_area_link
//////////////////////////////////////////////////////////////////
/*                      -END AREA MAP LINK-                     */
//////////////////////////////////////////////////////////////////

function transparentmaps_area_searchresults_link($listingID, $raw_url = false)
{
    global $config, $lang, $conn, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_areasearchresult_popup");
    $width = $recordSet->fields("addons_transparentmaps_areasearchresult_width");
    $height = $recordSet->fields("addons_transparentmaps_areasearchresult_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    // If a listing ID isn't set.. let them know something is wrong
    if ($listingID == '') {
        $display = 'No Listing ID';
        return $display;
    }
    if ($raw_url) {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_area_searchresults&printer_friendly=yes&listingID=' . $listingID;
        } else {
            $display = 'index.php?action=addon_transparentmaps_area_searchresults&listingID=' . $listingID;
        }
    } else {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_area_searchresults&printer_friendly=yes&listingID=' . $listingID . '" onclick="window.open(\'index.php?action=addon_transparentmaps_area&printer_friendly=yes&listingID=' . $listingID . '\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_area_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_area_searchresults&listingID=' . $listingID . '">' . $lang['tmaps_area_link'] . '</a>';
        }
    }
    return $display;
} // end function transparentmaps_area_link

//////////////////////////////////////////////////////////////////
/*                        -SHOW MAP LINK-                       */
//////////////////////////////////////////////////////////////////
function transparentmaps_showmap_link($listingID, $raw_url = false)
{
    global $config, $lang, $conn, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_showmap_popup");
    $width = $recordSet->fields("addons_transparentmaps_showmap_width");
    $height = $recordSet->fields("addons_transparentmaps_showmap_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    // If a listing ID isn't set.. let them know something is wrong
    if ($listingID == '') {
        $display = 'No Listing ID';
        return $display;
    }
    if ($raw_url) {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_showmap&printer_friendly=yes&listingID=' . $listingID;
        } else {
            $display = 'index.php?action=addon_transparentmaps_showmap&listingID=' . $listingID;
        }
    } else {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showmap&printer_friendly=yes&listingID=' . $listingID . '" onclick="window.open(\'index.php?action=addon_transparentmaps_showmap&printer_friendly=yes&listingID=' . $listingID . '\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_showmap_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showmap&listingID=' . $listingID . '">' . $lang['tmaps_showmap_link'] . '</a>';
        }
    }
    return $display;
} // end function transparentmaps_showmap_link
//////////////////////////////////////////////////////////////////
/*                      -END SHOW MAP LINK-                     */
//////////////////////////////////////////////////////////////////

function transparentmaps_showmap_searchresults_link($listingID, $raw_url = false)
{
    global $config, $lang, $conn, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_popup");
    $width = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_width");
    $height = $recordSet->fields("addons_transparentmaps_listingmapsearchresult_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    // If a listing ID isn't set.. let them know something is wrong
    if ($listingID == '') {
        $display = 'No Listing ID';
        return $display;
    }
    if ($raw_url) {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_showmap_searchresults&printer_friendly=yes&listingID=' . $listingID;
        } else {
            $display = 'index.php?action=addon_transparentmaps_showmap_searchresults&listingID=' . $listingID;
        }
    } else {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showmap_searchresults&printer_friendly=yes&listingID=' . $listingID . '" onclick="window.open(\'index.php?action=addon_transparentmaps_showmap&printer_friendly=yes&listingID=' . $listingID . '\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_showmap_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showmap_searchresults&listingID=' . $listingID . '">' . $lang['tmaps_showmap_link'] . '</a>';
        }
    }
    return $display;
} // end function transparentmaps_showmap_link

//////////////////////////////////////////////////////////////////
/*                        -USER MAP LINK-                       */
//////////////////////////////////////////////////////////////////
function transparentmaps_user_link($userID, $raw_url = false)
{
    global $config, $lang, $conn, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_user_popup");
    $width = $recordSet->fields("addons_transparentmaps_user_width");
    $height = $recordSet->fields("addons_transparentmaps_user_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    // If a User ID isn't set.. let them know something is wrong
    if ($userID == '') {
        $display = 'No User ID';
        return $display;
    }
    if (!$raw_url) {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_user&printer_friendly=yes&userID=' . $userID . '" onclick="window.open(\'index.php?action=addon_transparentmaps_user&printer_friendly=yes&userID=' . $userID . '\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_user_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_user&userID=' . $userID . '">' . $lang['tmaps_user_link'] . '</a>';
        }
    } else {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_user&printer_friendly=yes&userID=' . $userID;
        } else {
            $display = 'index.php?action=addon_transparentmaps_user&userID=' . $userID;
        }
    }
    return $display;
} // end function transparentmaps_user_link
//////////////////////////////////////////////////////////////////
/*                      -END USER MAP LINK-                     */
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
/*                        -SHOW ALL LINK-                       */
//////////////////////////////////////////////////////////////////
function transparentmaps_showall_link($raw_url = false)
{
    global $config, $lang, $conn, $current_ID, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_showall_popup");
    $width = $recordSet->fields("addons_transparentmaps_showall_width");
    $height = $recordSet->fields("addons_transparentmaps_showall_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    if (!$raw_url) {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showall&printer_friendly=yes" onclick="window.open(\'index.php?action=addon_transparentmaps_showall&printer_friendly=yes\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_showall_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showall">' . $lang['tmaps_showall_link'] . '</a>';
        }
    } else {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_showall&printer_friendly=yes';
        } else {
            $display = 'index.php?action=addon_transparentmaps_showall';
        }
    }
    return $display;
} // end function transparentmaps_showall_link

function transparentmaps_showall_link_pclass($pclass, $raw_url = false)
{
    global $config, $lang, $conn, $current_ID, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '';
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    $usepopup = $recordSet->fields("addons_transparentmaps_showall_popup");
    $width = $recordSet->fields("addons_transparentmaps_showall_width");
    $height = $recordSet->fields("addons_transparentmaps_showall_height");
    $apikey = $recordSet->fields("addons_transparentmaps_apikey");
    $popupwidth = $width + '20';
    $popupheight = $height + '20';
    if (!$raw_url) {
        if ($usepopup == '1') {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showall&pclass[]=' . $pclass . '&printer_friendly=yes" onclick="window.open(\'index.php?action=addon_transparentmaps_showall&pclass[]=' . $pclass . '&printer_friendly=yes\',\'\',\'width=' . $popupwidth . ',height=' . $popupheight . '\');return false;">' . $lang['tmaps_showall_link'] . '</a>';
        } else {
            $display = '<a class="tmap_link" href="index.php?action=addon_transparentmaps_showall&pclass[]=' . $pclass . '">' . $lang['tmaps_showall_link'] . '</a>';
        }
    } else {
        if ($usepopup == '1') {
            $display = 'index.php?action=addon_transparentmaps_showall&pclass[]=' . $pclass . '&printer_friendly=yes';
        } else {
            $display = 'index.php?action=addon_transparentmaps_showall&pclass[]=' . $pclass;
        }
    }
    return $display;
} // end function transparentmaps_showall_link

//////////////////////////////////////////////////////////////////
/*                      -END SHOW ALL LINK-                     */
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
/*                      -GEOCODE ALL LISTINGS-                  */
//////////////////////////////////////////////////////////////////
function transparentmaps_geocode_all($refresh = 'no')
{
    global $config, $lang, $conn, $jscript;
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    $misc = new misc();
    $display = '<div class="card card-frame mb-4">
	<div class="card-body py-2">';
    $geofailed = 0;
    $geocount = 0;
    $failed_listingID = [];
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $security = $login->loginCheck('edit_site_config', true);
    if ($security === true) {
        $misc->log_action("$lang[tmaps_log_start_geocode]");
        // GET LATITUDE AND LONGITUDE FIELD NAMES
        // Get Name of Lat Field
        $sql = "SELECT listingsformelements_field_name FROM " . $config['table_prefix'] . "listingsformelements 
				WHERE listingsformelements_field_type  = 'lat'";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $lat_field = $recordSet->fields('listingsformelements_field_name');
        // Get Name of Long Field
        $sql = "SELECT listingsformelements_field_name FROM " . $config['table_prefix'] . "listingsformelements 
				WHERE listingsformelements_field_type  = 'long'";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
        $long_field = $recordSet->fields('listingsformelements_field_name');
        if ($refresh == "no") {
            //// END GET LATITUDE AND LONGITUDE FIELD NAMES
            $encodeme = "SELECT DISTINCT listingsdb_id FROM " . $config['table_prefix'] . "listingsdbelements 
						WHERE listingsdb_id 
						NOT IN (SELECT DISTINCT listingsdb_id 
								FROM " . $config['table_prefix'] . "listingsdbelements 
								WHERE listingsdbelements_field_name = '" . $lat_field . "' 
								AND listingsdbelements_field_value <> '') 
								OR listingsdb_id 
								NOT IN (SELECT DISTINCT listingsdb_id 
										FROM " . $config['table_prefix'] . "listingsdbelements 
										WHERE listingsdbelements_field_name = '" . $long_field . "' 
										AND listingsdbelements_field_value <> ''
								)";
        } elseif ($refresh == "yes") {
            $encodeme = "SELECT DISTINCT listingsdb_id FROM " . $config['table_prefix'] . "listingsdb";
        }
        $recordSet1 = $conn->Execute($encodeme);
        if (!$recordSet1) {
            $misc->log_error($encodeme);
        }
        while (!$recordSet1->EOF) {
            $listingID = $recordSet1->fields['listingsdb_id'];
            $geo = geocode_listing($listingID, $refresh);
            if ($geo['encoded'] == 'no') {
                $geofailed++;
                $failed_listingID[$listingID] = $geo['error_text'];
            } else {
                $geocount++;
            }
            $recordSet1->MoveNext();
        } // end while
        $geototal = ($geocount + $geofailed);
        $show_info = "$lang[tmaps_log_end_geocode] $lang[tmaps_log_attempted]$geototal - $lang[tmaps_log_successful]$geocount - $lang[tmaps_log_failed]$geofailed <br />";
        $misc->log_action("$show_info");
        $display .= $show_info;
        foreach ($failed_listingID as $id => $text) {
            $display .= 'Listing: ' . $id . ' failed to geocode: ' . $text . '<br />';
        }
        $display .= '<br /><br /><br />';
    } else {
        $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
    }
    $display .= '</div></div>';
    return $display;
}

function geocode_listing($listingID, $refresh = 'no')
{
    global $config, $lang, $conn, $jscript, $api;
    require_once($config['basepath'] . '/include/misc.inc.php');
    $misc = new misc();
    $display = '';
    $load_geocoders = [];
    //Default Geo Settings
    $geo = ['latitude' => '', 'longitude' => '', 'encoded' => 'no'];
    //Load TransparentMaps Settings
    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    //Figure out the order to run the geocoders
    $google_priority = $recordSet->fields('addons_transparentmaps_google_geocoder_priority');
    $google_key = $recordSet->fields('addons_transparentmaps_apikey');
    $yahoo_priority = $recordSet->fields('addons_transparentmaps_yahoo_geocoder_priority');
    $yahoo_key = $recordSet->fields('addons_transparentmaps_yahookey');
    $other_priority = $recordSet->fields('addons_transparentmaps_show_photo');
    $other_key = $recordSet->fields('addons_transparentmaps_otherkey');
    if ($google_priority > 0 && trim($google_key) != '' && $google_key != 'enter Google API key') {
        $load_geocoders[$google_priority] = [0 => 'google_geocoder', 1 => $google_key];
    }
    if ($yahoo_priority > 0 && trim($yahoo_key) != '' && $yahoo_key != 'enter Yahoo! App ID') {
        $load_geocoders[$yahoo_priority] = [0 => 'yahoo_geocoder', 1 => $yahoo_key];
    }
    if ($other_priority > 0 && trim($other_key) != '' && $other_key != 'Alternate Geocoder Key') {
        $load_geocoders[$other_priority] = [0 => 'other_geocoder', 1 => $other_key];
    }
    ksort($load_geocoders);

    $default_country = $recordSet->fields("addons_transparentmaps_default_country");
    $show_all_classes = $recordSet->fields("addons_transparentmaps_area_allclasses");
    $display = '';
    //Get Listing Owner
    $sql = 'SELECT listingsdb_id, userdb_id 
			FROM ' . $config['table_prefix'] . 'listingsdb 
			WHERE listingsdb_id = ' . $listingID;
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $listing_agent = $recordSet->fields('userdb_id');
    if (intval($listing_agent) == 0) {
        return $geo;
    }

    // GET THIS LISTING'S LAT/LONG VALUES TO BUILD THE QUERY STRING
    // Get Name of Lat Field
    $sql = "SELECT listingsformelements_field_name 
			FROM " . $config['table_prefix'] . "listingsformelements 
			WHERE listingsformelements_field_type  = 'lat'";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }

    $lat_field = $recordSet->fields('listingsformelements_field_name');
    // Get Name of Long Field
    $sql = "SELECT listingsformelements_field_name 
			FROM " . $config['table_prefix'] . "listingsformelements 
			WHERE listingsformelements_field_type  = 'long'";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $long_field = $recordSet->fields('listingsformelements_field_name');
    if ($lat_field == '' || $long_field == '') {
        //Stop Geocoder no lat or long fields setup.
        return $geo;
    }

    // GET LATITUDE AND LONGITUDE FIELD VALUES
    // Get Latitude Value
    $lat = "SELECT listingsdbelements_field_value 
			FROM " . $config['table_prefix'] . "listingsdbelements 
			WHERE listingsdb_id = $listingID AND listingsdbelements_field_name = '" . $lat_field . "'";
    $recordSet = $conn->Execute($lat);
    if (!$recordSet) {
        $misc->log_error($lat);
    }
    $lat_value = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    if ($recordSet->RecordCount() == 0) {
        //Listing does not have a Lat field add it now..
        $sql = "INSERT INTO " . $config['table_prefix'] . "listingsdbelements (listingsdbelements_field_name,listingsdbelements_field_value,listingsdb_id,userdb_id) 
				VALUES ('$lat_field','',$listingID,$listing_agent)";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
    }
    // Get Longitude Value
    $long = "SELECT listingsdbelements_field_value FROM " . $config['table_prefix'] . "listingsdbelements 
			WHERE listingsdb_id = $listingID AND listingsdbelements_field_name = '" . $long_field . "'";
    $recordSet = $conn->Execute($long);
    if (!$recordSet) {
        $misc->log_error($long);
    }
    $long_value = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    if ($recordSet->RecordCount() == 0) {
        //Listing does not have a Lat field add it now..
        $sql = "INSERT INTO " . $config['table_prefix'] . "listingsdbelements (listingsdbelements_field_name,listingsdbelements_field_value,listingsdb_id,userdb_id) 
				VALUES ('$long_field','',$listingID,$listing_agent)";
        $recordSet = $conn->Execute($sql);
        if (!$recordSet) {
            $misc->log_error($sql);
        }
    }

    // GET ADDRESS
    $sql_address_field = $misc->make_db_safe($config['map_address']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $address = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add address field 2
    $sql_address_field = $misc->make_db_safe($config['map_address2']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $address2 = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add address field 3
    $sql_address_field = $misc->make_db_safe($config['map_address3']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $address3 = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add address field 4
    $sql_address_field = $misc->make_db_safe($config['map_address4']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $address4 = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add City
    $sql_address_field = $misc->make_db_safe($config['map_city']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $city = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add State
    $sql_address_field = $misc->make_db_safe($config['map_state']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $state = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add Zip Code
    $sql_address_field = $misc->make_db_safe($config['map_zip']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $zip = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    // Add Country
    $sql_address_field = $misc->make_db_safe($config['map_country']);
    $sql = "SELECT listingsdbelements_field_value, listingsformelements_field_type, listingsformelements_field_caption 
			FROM " . $config['table_prefix'] . "listingsdbelements, " . $config['table_prefix'] . "listingsformelements 
			WHERE ((" . $config['table_prefix'] . "listingsdbelements.listingsdb_id = $listingID) 
			AND (listingsformelements_field_name = listingsdbelements_field_name) 
			AND (listingsdbelements_field_name = $sql_address_field))";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $country = $misc->make_db_unsafe($recordSet->fields('listingsdbelements_field_value'));
    //// END GET ADDRESS
    $geocode_address = $address;
    if ($address2 != '') {
        $geocode_address .= ' ' . $address2;
    }
    if ($address3 != '') {
        $geocode_address .= ' ' . $address3;
    }
    if ($address4 != '') {
        $geocode_address .= ' ' . $address4;
    }
    $display_address = $geocode_address;
    if ($city != '') {
        $geocode_address .= ' ' . $city;
    }
    if ($state != '') {
        $geocode_address .= ', ' . $state;
    }
    if ($zip != '') {
        $geocode_address .= ' ' . $zip;
    }
    if ($country != '') {
        $geocode_address .= ' ' . $country;
    } else {
        $geocode_address .= ' ' . $default_country;
        $country = $default_country;
    }
    // Check if we have Lat/Long data
    // If there is no Lat/Long  Geocode it
    if ($refresh == 'no' && $lat_value != 0 && $long_value != 0) {
        $geo = [];
        $geo['encoded'] = 'yes';
        $geo['latitude'] = $lat_value;
        $geo['longitude'] = $long_value;
    } elseif (!empty($geocode_address)) {
        foreach ($load_geocoders as $priority => $geoarray) {
            $geocoder = $geoarray[0];
            require_once($config['basepath'] . '/addons/transparentmaps/geocoders/' . $geocoder . '.inc.php');
            //echo 'Trying Geocoder '.$geocoder;
            //echo "$address, $address2, $address3, $address4, $city, $state, $zip, $country, $geoarray[1]";
            $geo = $geocoder($address, $address2, $address3, $address4, $city, $state, $zip, $country, trim($geoarray[1]));
            //echo '<pre>'.print_r($geo,TRUE).'</pre>';
            $lat_value = $geo['latitude'];
            $long_value = $geo['longitude'];
            if ($geo['encoded'] == 'yes') {
                // Since we had to geocode this data - Load it into the database to speed up future queries
                // Load the Latitude Data into the listing
                $sql = "UPDATE " . $config['table_prefix'] . "listingsdbelements SET listingsdbelements_field_value = '" . $lat_value . "' 
						WHERE (listingsdbelements_field_name = '" . $lat_field . "') 
						AND (listingsdb_id = '" . $listingID . "')";
                $recordSet = $conn->Execute($sql);
                if ($recordSet == false) {
                    $misc->log_error($sql);
                }
                // Load the Longitude data into the listing
                $sql = "UPDATE " . $config['table_prefix'] . "listingsdbelements SET listingsdbelements_field_value = '" . $long_value . "' 
						WHERE (listingsdbelements_field_name = '" . $long_field . "') 
						AND (listingsdb_id = '" . $listingID . "')";
                $recordSet = $conn->Execute($sql);
                if ($recordSet == false) {
                    $misc->log_error($sql);
                }
                //We geocoded so skip other geocoders
                break;
            }
        }
    }
    $geo['display_address'] = $display_address;
    $geo['city'] = $city;
    $geo['state'] = $state;
    $geo['zip'] = $zip;
    $geo['country'] = $country;
    return $geo;
}
//////////////////////////////////////////////////////////////////
/*                     -END GEOCODE ALL LISTINGS-               */
//////////////////////////////////////////////////////////////////