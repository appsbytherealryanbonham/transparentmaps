/**
 * Initializes Google Map
 * @param  {[type]} map_name
 * @param  {[type]} qstring
 * @param  {[type]} move_listener
 * @return {[type]}
 */
function tmaps_init(map_name, qstring, move_listener)
{
    qstring = typeof qstring !== 'undefined' ? qstring : null;
    move_listener = typeof move_listener !== 'undefined' ? move_listener : false;

    tmaps_maps[map_name] = new google.maps.Map(document.getElementById(map_name), tmaps_options[map_name]["maps"]);
    if (move_listener == 0) {
        //alert(tmaps_markerURL+qstring);
        $.get(
            tmaps_markerURL + qstring,
            function (data) {
                processJSONMarkers(map_name, data);
                setAutoZoom(map_name);
            },
            "json"
        );
    } else {
        add_listener_map_movement(map_name);
    }

    //Set Street View Container
    tmaps_maps[map_name].setStreetView(tmaps_options[map_name]["streetview"]);
    tmaps_options[map_name]["directionsDisplay"].setMap(tmaps_maps[map_name]);
    tmaps_options[map_name]["directionsDisplay"].setPanel(document.getElementById(map_name + "directions"));
    tmaps_options[map_name]["geocoder"] = new google.maps.Geocoder();
    tmaps_options[map_name]["directionsService"] = new google.maps.DirectionsService();

    //Deal with KML Overlays
    tmaps_options[map_name]["kml_overlays"].forEach(function (entry) {
        //          console.log('Adding Overlay for '+entry);
        entry.setMap(tmaps_maps[map_name]);
    });

    //alert(qstring);
    // tmaps_maps[map_name].controls[google.maps.ControlPosition.RIGHT].push(tmaps_pBar[map_name].getDiv());
}
/**
 * Clears All Markers From Map
 * @param  {[type]} map_name
 * @return {[type]}
 */
function clear_markers(map_name)
{
    for (var i = 0; i < tmaps_options[map_name]["marker"].list.length; i++) {
        tmaps_options[map_name]["marker"].list[i].setMap(null);
    }
    tmaps_options[map_name]["marker"].list = [];
}
/**
 * Sets Correct Zoom Level on Map
 * @param {[type]} map_name
 */
function setAutoZoom(map_name)
{
    maxZoomService = new google.maps.MaxZoomService();
    var myPointCount = tmaps_options[map_name]["marker"].list.length;
    console.log('Number of Markers On Map ' + myPointCount);
    var myCenter = tmaps_options[map_name]["bounds"].getCenter();
    //      console.log('Boundary Center '+myCenter);

    maxZoomService.getMaxZoomAtLatLng(myCenter, function (response) {
        if (response.status != google.maps.MaxZoomStatus.OK) {
            console.log('Error in MaxZoomService');
        } else {
            var azfind = tmaps_options[map_name]["autozoom"].indexOf('az');

            //console.log('azfind '+azfind +' - '+tmaps_options[map_name]["autozoom"]);
            if (tmaps_options[map_name]["autozoom"] == 'az') {
                //                  console.log("AutoZooming to Max Zoom of " + response.zoom)
                tmaps_maps[map_name].panTo(myCenter);
                tmaps_maps[map_name].setZoom(getZoomByBounds(tmaps_maps[map_name], tmaps_options[map_name]["bounds"], response.zoom));
            } else if (azfind != -1) {
                var myzoom = tmaps_options[map_name]["autozoom"].match(/az(\d+)/);
                //              console.log("AutoZooming to Max Zoom of " + response.zoom+" - " + myzoom[1])
                tmaps_maps[map_name].panTo(myCenter);
                tmaps_maps[map_name].setZoom(getZoomByBounds(tmaps_maps[map_name], tmaps_options[map_name]["bounds"], response.zoom) - myzoom[1]);
            } else {
                //              console.log("StandardZoom to Zoom of " + tmaps_options[map_name]["autozoom"])
                tmaps_maps[map_name].panTo(myCenter);
                tmaps_maps[map_name].setZoom(tmaps_options[map_name]["autozoom"]);
            }
        }

    });
}
/**
* Returns the zoom level at which the given rectangular region fits in the map view.
* The zoom level is computed for the currently selected map type.
* @param {google.maps.Map} map
* @param {google.maps.LatLngBounds} bounds
* @return {Number} zoom level
**/
function getZoomByBounds(map, bounds, MAX_ZOOM)
{
    var mapTypes = map.mapTypes;
    var MIN_ZOOM = 0;
    MAX_ZOOM = typeof MAX_ZOOM !== 'undefined' ? MAX_ZOOM : 21;

    var ne = map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
    var sw = map.getProjection().fromLatLngToPoint(bounds.getSouthWest());

    var worldCoordWidth = Math.abs(ne.x - sw.x);
    var worldCoordHeight = Math.abs(ne.y - sw.y);

    //Fit padding in pixels
    var FIT_PAD = 40;

    for (var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom) {
        if (worldCoordWidth * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).width() &&
            worldCoordHeight * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).height()) {
            //          console.log("Zoom By Bounds Level "+zoom);
            return zoom;
        }
    }
    return 0;
}
/**
 * PHP strpos() implimentation in js
 * @param  {[type]} haystack
 * @param  {[type]} needle
 * @param  {[type]} offset
 * @return {[type]}
 */
function strpos(haystack, needle, offset)
{
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}
/**
 * Processes the json output from data.xml.php and adds the markers to the map
 * @param  {[type]} map_name
 * @param  {[type]} jsonDoc
 * @return {[type]}
 */
function processJSONMarkers(map_name, jsonDoc)
{
    //Clear Markers
    console.log(jsonDoc);
    clear_markers(map_name);
    tmaps_options[map_name]["bounds"] = new google.maps.LatLngBounds();
    //Loop Through each Marker
    for (var i = 0; i < jsonDoc.length; i++) {
        var lat = parseFloat(jsonDoc[i]["lat"]);
        var lng = parseFloat(jsonDoc[i]["lng"]);
        var label = jsonDoc[i]["label"];
        var html = jsonDoc[i]["html"];
        var icon = jsonDoc[i]["icon"];
        var seourl = jsonDoc[i]["seourl"];
        var view_listing_text = jsonDoc[i]["view_listing_text"];
        add_marker(map_name, lat, lng, label, html, icon, seourl, view_listing_text)
    }
}
/**
 * Creates Marker and Adds to to the map along with the infowindow and
 * extends the current map boundary to include the new marker
 * @param {[type]} map_name
 * @param {[type]} latitude
 * @param {[type]} longitude
 * @param {[type]} title
 * @param {[type]} html
 * @param {[type]} icon
 */
function add_marker(map_name, latitude, longitude, title, html, icon, seourl, view_listing_text)
{
    icon = typeof icon !== 'undefined' ? icon : 0;
    //alert(map_name);
    var point = new google.maps.LatLng(latitude, longitude);
    var myicon = {
        url: tmaps_iconURL + '/' + icon + '.png',
        size: tmaps_options[map_name]["marker"].iconSize,
        anchor: new google.maps.Point(6, 20)
    }
    var marker_num = tmaps_options[map_name]["marker"].list.length;
    tmaps_options[map_name]["marker"].list[marker_num] = new google.maps.Marker({
        position: point,
        map: tmaps_maps[map_name],
        title: title,
        icon: myicon
    });
    //Extend Map Boundary
    tmaps_options[map_name]["bounds"].extend(point);
    //  console.log("Extending Bounds by "+point);

    var or_listingid = getUrlVars()["listingID"];
    var printer_friendly = getUrlVars()["printer_friendly"];
    var or_action = getUrlVars()["action"];


    switch (or_action) {
        case 'searchresults':
            if (printer_friendly == 'yes') {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' + seourl + '\');window.close();return false;">' + view_listing_text + '</a>');
            } else {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="' + seourl + '">' + view_listing_text + '</a>');
            }
            break;
        case 'listingview':
            if (printer_friendly == 'yes') {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' + seourl + '\');window.close();return false;">' + view_listing_text + '</a>');
            }
            if (icon == 'center' || tmaps_options[map_name]["marker"].list.length == 1) {
                html = html.replace('TLINK', '');
            } else {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="' + seourl + '">' + view_listing_text + '</a>');
            }
            break;
        case 'addon_transparentmaps_area':
            if (printer_friendly == 'yes' && icon != 'center') {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' + seourl + '\');window.close();return false;">' + view_listing_text + '</a>');
            } else {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="window.close();return false;">' + view_listing_text + '</a>');
            }
            break;
        case 'addon_transparentmaps_showmap':
            if (printer_friendly == 'yes') {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' + seourl + '\');window.close();return false;">' + view_listing_text + '</a>');
            }
            break;
        case 'addon_transparentmaps_user':
            if (printer_friendly == 'yes') {
                html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' + seourl + '\');window.close();return false;">' + view_listing_text + '</a>');
            }
            break;
        default:
            html = html.replace('TLINK', '<br /><a class="tmap_link_info" href="' + seourl + '">' + view_listing_text + '</a>');
            break;
    }


    if (tmaps_options[map_name]["show_directions"] == true) {
        html = html.replace(/width:100px;/g, "width:50px;");
        html = html +
            "<div style=\"clear:both; text-align: center;\">" +
            "Your address: <input id='" + map_name + "clientAddress' type='text'><br />" +
            "<input type='button' onClick=getDirctions('" + map_name + "'," + map_name + "clientAddress.value,null," + marker_num + ") value='From Your Address To Here'><br />" +
            "<input type='button' onClick=getDirctions('" + map_name + "',null," + map_name + "clientAddress.value," + marker_num + ") value='To Your Address From Here'><br />"
    }

    var infowindow = new google.maps.InfoWindow({
        content: html
    });

    google.maps.event.addListener(tmaps_options[map_name]["marker"].list[marker_num], 'click', function () {
        infowindow.open(tmaps_maps[map_name], tmaps_options[map_name]["marker"].list[marker_num]);
    });

}

/**
 * returns any set GET vars
 */
function getUrlVars()
{
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}
/**
 * Handles Map Movement by user, and calls data.xml.php and updates map with correct markers
 * @param {[type]} map_name
 */
function add_listener_map_movement(map_name)
{
    tmaps_options[map_name]["listeners"].movelistner = google.maps.event.addListener(tmaps_maps[map_name], "idle", function () {
        var latitude = tmaps_maps[map_name].getCenter().lat();
        var longitude = tmaps_maps[map_name].getCenter().lng();
        //TODO: Get Pclass URL

        //TODO: Handle Printer friendly.. ??
        var dataURL = tmaps_markerURL + "latlong_dist_lat=" + latitude + "&latlong_dist_long=" + longitude + "&latlong_dist_dist=" + tmaps_centers[map_name]['distance'] + "&latlong_dist_lat_orig=" + tmaps_centers[map_name]['lat'] + "&latlong_dist_long_orig=" + tmaps_centers[map_name]['long'];
        $.get(
            dataURL,
            function (data) {
                processJSONMarkers(map_name, data);
                //setAutoZoom(map_name);
            },
            "json"
        );
    });
}
/**
 * Get Directions from or to a address to a marker location.
 * @param  {[type]} map_name [description]
 * @param  {[type]} from     [description]
 * @param  {[type]} to       [description]
 * @param  {[type]} marker   [description]
 * @return {[type]}          [description]
 */
function getDirctions(map_name, from, to, marker)
{
    //  console.log("Get Directions From " + from + " To " + to + " Marker "+marker);
    marker_point = tmaps_options[map_name]["marker"].list[marker]["position"];
    //  console.log("Marker Point "+marker_point);
    if (from == null) {
        //Get Directions To our Client Specified Address
        tmaps_options[map_name]["geocoder"].geocode(
            {
                'address': to
            },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var origin = marker_point;
                    var destination = results[0].geometry.location;

                    var request = {
                        origin: origin,
                        destination: destination,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };

                    tmaps_options[map_name]["directionsService"].route(request, function (response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            tmaps_options[map_name]["directionsDisplay"].setDirections(response);
                            //console.dir(response);
                        }
                    });
                } else {
                    document.getElementById('clientAddress').value =
                        "Directions cannot be computed at this time.";
                }
            }
        );
    } else {
        //Get Directions To our Client Specified Address
        tmaps_options[map_name]["geocoder"].geocode(
            {
                'address': from
            },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var origin = results[0].geometry.location;
                    var destination = marker_point;
                    var request = {
                        origin: origin,
                        destination: destination,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    };
                    tmaps_options[map_name]["directionsService"].route(request, function (response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            tmaps_options[map_name]["directionsDisplay"].setDirections(response);
                            //console.dir(response);
                        }
                    });
                } else {
                    alert("Directions cannot be computed at this time.");
                }
            }
        );
    }
}