<?php
if (!class_exists('formGeneration')) {
    class formGeneration
    {
        public function startform($action, $method = 'post', $enctype = 'multipart/form-data', $name = '')
        {
            if ($name == '') {
                $formitem = '<form action="' . $action . '" enctype="' . $enctype . '" method="' . $method . '">';
            } else {
                $formitem = '<form action="' . $action . '" enctype="' . $enctype . '" method="' . $method . '" name="' . $name . '">';
            }

            return $formitem;
        } // End function startform()
        public function createformitem($type, $name = '', $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            // $options needs to be an array value,caption
            $formitem = 'create_' . $type;
            $formitem = $this->$formitem($name, $value, $multiple, $size, $src, $checked, $rows, $cols, $options, $selected_item, $disabled);
            $formitem .= "\r\n";
            return $formitem;
        } // End function startform()
        public function endform()
        {
            $formitem = '</form>';
            return $formitem;
        } // End function endform()
        // These functions should not be called direct
        public function create_text($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            if ($disabled) {
                $formitem = '<input type="text" class="form-control" id="' . $name . '" name="' . $name . '" value="' . $value . '" size="' . $size . '" disabled="disabled" />';
            } else {
                $formitem = '<input type="text" class="form-control" id="' . $name . '"  name="' . $name . '" value="' . $value . '" size="' . $size . '" />';
            }
            return $formitem;
        }
        public function create_lockedtext($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input type="text"  class="form-control" id="' . $name . '" name="' . $name . '" value="' . $value . '" size="' . $size . '" onfocus="this.blur()" />';
            return $formitem;
        }
        public function create_password($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input type="password"  class="form-control" id="' . $name . '" name="' . $name . '" value="' . $value . '" size="' . $size . '" />';
            return $formitem;
        }
        public function create_hidden($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input type="hidden" class="form-control" id="' . $name . '" name="' . $name . '" value="' . $value . '" />';
            return $formitem;
        }
        public function create_submit($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            if ($value == '') {
                $value = 'submit';
            }
            $formitem = '<input type="submit" class="btn btn-primary float-end"  value="' . $value . '"';
            if ($src != '') {
                $formitem .= ' src="' . $src . '"';
            }
            $formitem .= ' />';
            return $formitem;
        }
        public function create_checkbox($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input class="form-check-input" id="' . $name . '" type="checkbox" name="' . $name . '" value="' . $value . '"';
            if ($checked == true) {
                $formitem .= ' checked="checked"';
            }
            $formitem .= ' />';
            return $formitem;
        }
        public function create_lockedcheckbox($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input  class="form-check-input" id="' . $name . '"  type="checkbox" name="' . $name . '" value="' . $value . '"';
            if ($checked == true) {
                $formitem .= ' checked="checked"';
            }
            $formitem .= ' onfocus="this.blur()" />';
            return $formitem;
        }
        public function create_textarea($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<textarea class="form-control" id="' . $name . '" rows="' . $rows . '" cols="' . $cols . '"  name="' . $name . '">' . $value . '</textarea>';
            return $formitem;
        }
        public function create_select($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<select class="form-control" id="' . $name . '" name="' . $name . '"';
            if ($multiple == true) {
                $formitem .= ' multiple="multiple"';
                $formitem .= ' size="' . $size . '"';
            }
            $formitem .= '>';
            if ($options != '') {
                foreach ($options as $k => $v) {
                    if (is_array($selected_item)) {
                        if (in_array($k, $selected_item)) {
                            $formitem .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
                        } else {
                            $formitem .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    } else {
                        if ($k == $selected_item) {
                            $formitem .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
                        } else {
                            $formitem .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }
                }
            }
            $formitem .= '</select>';
            return $formitem;
        }
        public function create_radio($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input class="form-check-input" id="' . $name . '" type="radio" name="' . $name . '" value="' . $value . '"';
            if ($checked == true) {
                $formitem .= ' checked="checked"';
            }
            $formitem .= ' />';
            return $formitem;
        }
        public function create_file($name, $value = '', $multiple = false, $size = 25, $src = '', $checked = false, $rows = 0, $cols = 0, $options = '', $selected_item = '', $disabled = false)
        {
            $formitem = '<input class="form-control" id="' . $name . '" type="file" name="' . $name . '" />';
            return $formitem;
        }
    } // End formGeneration class
} //End if (!class exisists)
