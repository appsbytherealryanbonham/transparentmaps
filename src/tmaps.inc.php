<?php
class transparentmaps_googleapi
{
    public $apikey; // This is your google API key
    //TODO v3
    public $autobounds; //tells it if we should use the code to handle the auto zoom or not. Want to look into
    public $map_width = 500; // The map width
    public $map_height = 500; //The map height
    public $query_string; //This is the query string to pass the the data.xml.php file.
    public $show_small_map_controls =1;
    public $show_map_type_controls = ['G_NORMAL_MAP','G_SATELLITE_MAP','G_HYBRID_MAP','G_PHYSICAL_MAP'];
    public $show_map_scale = 0;
    //Remove v3
    public $center_lat = '39.368279';
    //Remove v3
    public $center_long = '-100.195312';
    public $default_zoom = 14;
    public $default_map_type = 'G_NORMAL_MAP';
    public $distance=0;
    public $language='en';
    //Remove v3
    public $show_local_search=0;
    //TODO v3
    public $show_directions=0;
    public $show_streetview=0;
    //TODO v3
    public $show_pano='NONE';
    //Remove v3
    public $ScrollWheelZoom = 1;
    //Remove v3
    public $enableContinuousZoom=1;
    public $enableDragging=1;
    public $map_name = 'maptr';
    public $icon_width = 12;
    public $icon_height = 20;
    //Remove v3
    public $shadow_width = 22;
    //Remove v3
    public $shadow_height = 20;
    //Remove v3
    public $iconfolder = null;
    public $iconfolderURL = null;
    public $dataXmlFolder=null;
    public $js_folderURL = null;
    public $css_folderURL = null;
    //TODO v3
    public $kml_overlay_urls = [];
    //Remove v3
    public $origional_lat_value='';
    //Remove v3
    public $origional_long_value='';
    //Remove v3
    public $show_progress_bar = 1;
    //Pclass
    public $pclass=[];
    //lang
    public $lang_unable_to_locate_message = 'Sorry, Unable to map the location';
    public $lang_avoid_highways = 'Avoid Highways';
    public $lang_walk = 'Walk';
    public $lang_to_here = 'To here';
    public $lang_from_here = 'From here';
    public $lang_directions = 'Directions:';
    public $lang_start_address = 'Start address:';
    public $lang_end_address = 'End address:';
    public $lang_get_directions = 'Get Directions:';
    public $lang_loading_properties = 'Loading Properties, please wait..';


    public function call_google_api()
    {
        //Load Google Ajax API
        return '
    	<script  src="https://maps.googleapis.com/maps/api/js?key='.$this->apikey.'&language='.$this->language.'" type="text/javascript"></script>

    	<script type="text/javascript">

    		var tmaps_maps = [];
    		var tmaps_pBar = [];
    		var tmaps_options = [];
    		var tmaps_centers = [];
    		var tmaps_markers = [];
    		var tmaps_infowindows = [];
    		var tmaps_markerURL = "' . $this->dataXmlFolder . '/data.xml.php?";
    		var tmaps_iconURL = "'.$this->iconfolderURL.'";
    		var tmaps_types = [];

    	</script>
    	<script type="text/javascript" src="'.$this->js_folderURL.'/progressBar.js"></script>
    	<script src="'.$this->js_folderURL.'/tmaps.js" type="text/javascript"></script>
    	<script type="text/javascript">

			function to_old_win(url){
				window.opener.location=url;
			}

		</script>
		<link href="'.$this->css_folderURL.'/default.css" type="text/css" rel="stylesheet" />
    	';
    }

    public function preload_icons()
    {
        $html='';
        //Preload all icon file
        if ($handle = opendir($this->iconfolder)) {
            while (false !== ($file = readdir($handle))) {
                if (strpos($file, '.png') !== false) {
                    if (is_file($this->iconfolder . '/' . $file)) {
                        $html .= '<img src="' . $this->iconfolderURL . '/' . $file . '" style="display:none" alt="Marker Icon ' . $file . '" />';
                    }
                }
            }
            closedir($handle);
        }
        return $html;
    }
    public function create_google_map_div()
    {
        $html = '<div id="'.$this->map_name.'" style="width: ' . $this->map_width . 'px; height: ' . $this->map_height . 'px; font-size:10px; text-align:left;">' . $this->lang_unable_to_locate_message . '</div>';
        return $html;
    }
    public function create_google_map($use_movement_listner = false)
    {
        //Create a div for the map.

        //('G_NORMAL_MAP','G_SATELLITE_MAP','G_HYBRID_MAP','G_PHYSICAL_MAP','G_SATELLITE_3D_MAP');

        //Start of Map
        $jscript_last = '
		<script>


		tmaps_maps["'.$this->map_name.'"] = null;
		tmaps_pBar["'.$this->map_name.'"] = new progressBar();
		tmaps_markers["'.$this->map_name.'"] = [];
		
    	tmaps_centers["'.$this->map_name.'"] = [];
		tmaps_infowindows["'.$this->map_name.'"] = [];
		tmaps_centers["'.$this->map_name.'"]["lat"] = '.$this->center_lat.';
		tmaps_centers["'.$this->map_name.'"]["long"] = '.$this->center_long.';
		tmaps_centers["'.$this->map_name.'"]["distance"] = '.$this->distance.';

		tmaps_options["'.$this->map_name.'"] = []
		//Load Icon Options
		tmaps_options["'.$this->map_name.'"]["marker"] = {}
		tmaps_options["'.$this->map_name.'"]["marker"].list = []
		tmaps_options["'.$this->map_name.'"]["listeners"] = {}
		tmaps_options["'.$this->map_name.'"]["kml_overlays"] = []
		tmaps_options["'.$this->map_name.'"]["autozoom"] = "'.$this->autobounds.'";
		tmaps_options["'.$this->map_name.'"]["bounds"] = null;
		tmaps_options["'.$this->map_name.'"]["show_directions"] = '.$this->show_directions.';
		tmaps_options["'.$this->map_name.'"]["directionsDisplay"] = new google.maps.DirectionsRenderer({
			suppressMarkers: true
		});
	
		tmaps_options["'.$this->map_name.'"]["marker"].iconSize = new google.maps.Size('.$this->icon_width.', '.$this->icon_height.');
		
		tmaps_options["'.$this->map_name.'"]["maps"] = {}
		tmaps_options["'.$this->map_name.'"]["maps"].logoPassive = true;
		tmaps_options["'.$this->map_name.'"]["maps"].center = new google.maps.LatLng('.$this->center_lat.', '.$this->center_long.');
		tmaps_options["'.$this->map_name.'"]["maps"].zoom = '.$this->default_zoom.';

		//var myLatlng = new google.maps.LatLng(0.0, 0.0);
    		tmaps_types["G_PHYSICAL_MAP"] = google.maps.MapTypeId.TERRAIN;
    		tmaps_types["G_NORMAL_MAP"] = google.maps.MapTypeId.ROADMAP;
    		tmaps_types["G_HYBRID_MAP"] = google.maps.MapTypeId.HYBRID;
    		tmaps_types["G_SATELLITE_MAP"] = google.maps.MapTypeId.SATELLITE;

		tmaps_options["'.$this->map_name.'"]["maps"].mapTypeId = tmaps_types["'.$this->default_map_type.'"];
		tmaps_options["'.$this->map_name.'"]["maps"].mapTypeControlOptions = {
			mapTypeIds: []
		};
		';
        //print_r($this->show_map_type_controls);
        foreach ($this->show_map_type_controls as $control) {
            $jscript_last .= 'tmaps_options["'.$this->map_name.'"]["maps"].mapTypeControlOptions.mapTypeIds.push(tmaps_types["'.$control.'"]);
			';
        }
        
        foreach ($this->kml_overlay_urls as $kml_overlay_url) {
            if (trim($kml_overlay_url)!='') {
                $jscript_last .= 'tmaps_options["'.$this->map_name.'"]["kml_overlays"].push(new google.maps.KmlLayer({ url: "'.trim($kml_overlay_url).'" }));
				';
            }
        }

        if ($this->show_streetview && $this->show_pano=='EXTDIV') {
            $jscript_last .= 'tmaps_options["'.$this->map_name.'"]["streetview"] = new google.maps.StreetViewPanorama(document.getElementById("'.$this->map_name.'panorama")	);
			';
        } else {
            $jscript_last .= 'tmaps_options["'.$this->map_name.'"]["streetview"] = null;
			';
        }

        $jscript_last .= '

		// Add Map controls
		var show_small_map_controls = '.$this->show_small_map_controls.';
		var show_map_scale = '.$this->show_map_scale.';
		var draggable = '.$this->enableDragging.';
		var scrollwheel = '.$this->ScrollWheelZoom.';
		if(draggable == 1){
			tmaps_options["'.$this->map_name.'"]["maps"].draggable = true;
		}else{
			tmaps_options["'.$this->map_name.'"]["maps"].draggable = false;
		}
		if(scrollwheel == 1){
			tmaps_options["'.$this->map_name.'"]["maps"].scrollwheel = true;
		}else{
			tmaps_options["'.$this->map_name.'"]["maps"].scrollwheel = false;
		}

		if (show_small_map_controls == 1) {
			tmaps_options["'.$this->map_name.'"]["maps"].zoomControl = true;
		    tmaps_options["'.$this->map_name.'"]["maps"].zoomControlOptions = {
			      style: google.maps.ZoomControlStyle.SMALL
				};
		} else {
			tmaps_options["'.$this->map_name.'"]["maps"].zoomControl = true;
		    tmaps_options["'.$this->map_name.'"]["maps"].zoomControlOptions = {
			      style: google.maps.ZoomControlStyle.LARGE
			    };
		}
		// Add Scale
		if (show_map_scale == 1) {
			tmaps_options["'.$this->map_name.'"]["maps"].scaleControl = true;
		}
		tmaps_init("'.$this->map_name.'","'.$this->query_string.'",'.intval($use_movement_listner).');


		</script>'."\r\n";

        return $jscript_last;
    }
}
