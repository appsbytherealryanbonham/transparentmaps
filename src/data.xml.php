<?php
    //Set Percision to 14
    @ini_set('precision', 14);

    global $config, $lang, $conn, $current_ID, $jscript;
    require_once('../../include/common.php');
    //Include Language Files
    // Determine which Language File to Use
    if (isset($_SESSION["users_lang"]) && $_SESSION["users_lang"] != $config['lang']) {
        require_once($config['basepath'] . '/include/language/' . $_SESSION['users_lang'] . '/lang.inc.php');
    } else {
        // Use Sites Defualt Language
        require_once($config['basepath'] . '/include/language/' . $config['lang'] . '/lang.inc.php');
    }

    require_once($config['basepath'] . '/include/search.inc.php');
    require_once($config['basepath'] . '/include/misc.inc.php');
    require_once($config['basepath'] . '/addons/transparentmaps/lang.php');
    require_once($config['basepath'] . '/addons/transparentmaps/addon.inc.php');
    require_once($config['basepath'] . '/include/listing.inc.php');

    $sql = "SELECT * FROM " . $config["table_prefix_no_lang"] . "addons_transparentmaps";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        echo "ERROR: " . $sql;
    }
    if (isset($_GET['tmaps_usepopup'])) {
        $usepopup = intval($_GET['tmaps_usepopup']);
        unset($_GET['tmaps_usepopup']);
    } else {
        $usepopup=0;
    }

    $show_address = $recordSet->fields("addons_transparentmaps_show_address");
    $show_city = $recordSet->fields("addons_transparentmaps_show_city");
    $show_state = $recordSet->fields("addons_transparentmaps_show_state");
    $show_zip = $recordSet->fields("addons_transparentmaps_show_zip");
    $show_photo = $recordSet->fields("addons_transparentmaps_show_photo");
    $show_title = $recordSet->fields("addons_transparentmaps_show_title");
    $text_0 = $recordSet->fields("addons_transparentmaps_text_0");
    $text_1 = $recordSet->fields("addons_transparentmaps_text_1");
    $text_2 = $recordSet->fields("addons_transparentmaps_text_2");
    $text_3 = $recordSet->fields("addons_transparentmaps_text_3");
    $text_4 = $recordSet->fields("addons_transparentmaps_text_4");
    $text_5 = $recordSet->fields("addons_transparentmaps_text_5");
    $text_6 = $recordSet->fields("addons_transparentmaps_text_6");
    $field_1 = $recordSet->fields("addons_transparentmaps_field_1");
    $field_2 = $recordSet->fields("addons_transparentmaps_field_2");
    $field_3 = $recordSet->fields("addons_transparentmaps_field_3");
    $field_4 = $recordSet->fields("addons_transparentmaps_field_4");
    $field_5 = $recordSet->fields("addons_transparentmaps_field_5");
    $field_6 = $recordSet->fields("addons_transparentmaps_field_6");
    $field_1_caption = $recordSet->fields("addons_transparentmaps_caption_field_1");
    $field_2_caption = $recordSet->fields("addons_transparentmaps_caption_field_2");
    $field_3_caption = $recordSet->fields("addons_transparentmaps_caption_field_3");
    $field_4_caption = $recordSet->fields("addons_transparentmaps_caption_field_4");
    $field_5_caption = $recordSet->fields("addons_transparentmaps_caption_field_5");
    $field_6_caption = $recordSet->fields("addons_transparentmaps_caption_field_6");
    $default_country = $recordSet->fields("addons_transparentmaps_default_country");
    $use_customurl = $recordSet->fields("addons_transparentmaps_use_customurl");
    $config_customurl = $recordSet->fields("addons_transparentmaps_customurl");
    $customurl = '';

    $a = 0;
    //header('Content-type: text/xml; charset=utf-8');
    header('Content-type: application/json; charset=utf-8');

    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    // echo "<?xml version=\"1.0\" encoding=\"utf-8\"\\\n";
    // echo "<markers>\n";

    // GET LATITUDE AND LONGITUDE FIELD NAMES
    // Get Name of Lat Field
    $sql = "SELECT listingsformelements_field_name 
			FROM " . $config['table_prefix'] . "listingsformelements 
			WHERE listingsformelements_field_type  = 'lat'";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $lat_field = $recordSet->fields('listingsformelements_field_name');
    // Get Name of Long Field
    $sql = "SELECT listingsformelements_field_name 
			FROM " . $config['table_prefix'] . "listingsformelements 
			WHERE listingsformelements_field_type  = 'long'";
    $recordSet = $conn->Execute($sql);
    if (!$recordSet) {
        $misc->log_error($sql);
    }
    $long_field = $recordSet->fields('listingsformelements_field_name');
    //// END GET LATITUDE AND LONGITUDE FIELD NAMES
    // GET ID'S FOR LISTINGS MATCHING OUR QUERY
    // Check if this is a distance search for the area function, if so get ALL of the listings within the distance
    if (isset($_GET['latlong_dist_lat_orig']) && isset($_GET['latlong_dist_long_orig'])) {
        $origional_marker_lat=$_GET['latlong_dist_lat_orig'];
        $origional_marker_long=$_GET['latlong_dist_long_orig'];
        unset($_GET['latlong_dist_lat_orig']);
        unset($_GET['latlong_dist_long_orig']);
    }
    $search_page = new search_page();

    if (isset($_GET['latlong_dist_lat']) && $_GET['latlong_dist_lat'] != '' && isset($_GET['latlong_dist_long']) && $_GET['latlong_dist_long'] != '') {
        $matched_listing_ids = $search_page->search_results(true);
    } elseif (isset($_GET['curpage']) && $_GET['curpage'] == 'all') {
        unset($_GET['curpage']);
        $matched_listing_ids = $search_page->search_results(true);
    } elseif (isset($_GET['user_ID']) && $_GET['user_ID'] != '') {
        $matched_listing_ids = $search_page->search_results(true);
    } else {
        $matched_listing_ids = $search_page->search_results('perpage');
    }

    $markers = [];
    $displaywidth = $config['thumbnail_width'];

    foreach ($matched_listing_ids as $v) {
        $pclass_list=[];
        $listingID = $v;
        $geo = geocode_listing($listingID);
        //print_r($geo);
        $display_address = utf8_encode($geo['display_address']);
        $city = utf8_encode($geo['city']);
        $state = utf8_encode($geo['state']);
        $zip = utf8_encode($geo['zip']);
        $country = utf8_encode($geo['country']);
        $lat_value=$geo['latitude'];
        $long_value= $geo['longitude'];

        //////////////////////////////////////////////////////////////////
        /*    GET THE FIELDS AND IMAGES TO BUILD THE INFO WINDOW        */
        //////////////////////////////////////////////////////////////////
        // IF SHOW PHOTO IS YES THEN GRAB THE THUMBNAIL IMAGE
        if ($show_photo == 1) {
            $sql = "SELECT listingsimages_thumb_file_name, listingsimages_id 
					FROM " . $config['table_prefix'] . "listingsimages 
					WHERE (listingsdb_id = $listingID) 
					ORDER BY listingsimages_rank";
            $recordSet = $conn->SelectLimit($sql, 1, 0);
            if ($recordSet === false) {
                log_error($sql);
            }
            $num_images = $recordSet->RecordCount();
            if ($num_images > 0) {
                while (!$recordSet->EOF) {
                    $thumb_file_name = $misc->make_db_unsafe($recordSet->fields('listingsimages_thumb_file_name'));
                    $imageID = $misc->make_db_unsafe($recordSet->fields('listingsimages_id'));
                    if (strpos($thumb_file_name, 'http://')===0||strpos($thumb_file_name, 'https://')===0) {
                        $thumb = '<img src="' . $thumb_file_name . '" style="width:' . $displaywidth . 'px;" alt="' . utf8_encode($thumb_file_name) . '" />';
                        $displaywidth = $config['thumbnail_width'];
                    } else {
                        if ($thumb_file_name != "" && file_exists("$config[listings_upload_path]/$thumb_file_name")) {
                            $imagedata = GetImageSize("$config[listings_upload_path]/$thumb_file_name");
                            $imagewidth = $imagedata[0];
                            $imageheight = $imagedata[1];
                            $shrinkage = $config['thumbnail_width'] / $imagewidth;
                            $displaywidth = $imagewidth * $shrinkage;
                            $displayheight = $imageheight * $shrinkage;
                            $thumb = '<img src="' . $config['listings_view_images_path'] . '/' . $thumb_file_name . '" style="width:' . $displaywidth . 'px;" alt="' . utf8_encode($thumb_file_name) . '" />';
                        } // end if ($thumb_file_name != "")
                    }
                    $recordSet->MoveNext();
                } // end while
            } else {
                if ($config['show_no_photo'] == 1) {
                    $displaywidth = $config['thumbnail_width'];
                    $thumb = '<img src="' . $config['baseurl'] . '/images/nophoto.gif" style="width:' . $displaywidth . 'px;" alt="' . utf8_encode($lang['no_photo']) . '" />';
                } else {
                    $thumb = '';
                    $displaywidth = $config['thumbnail_width'];
                }
            }
        }

        $listing_pages = new listing_pages();

        // IF SHOW TITLE IS YES THEN GET THE TITLE
        if ($show_title == 1) {
            $title = utf8_encode($listing_pages->get_title($listingID));
        } else {
            $title = '';
        }
        
        // GET THE OPTIONAL FIELDS
        $field_1_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_1, $field_1_caption));
        $field_2_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_2, $field_2_caption));
        $field_3_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_3, $field_3_caption));
        $field_4_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_4, $field_4_caption));
        $field_5_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_5, $field_5_caption));
        $field_6_value = utf8_encode($listing_pages->renderSingleListingItem($listingID, $field_6, $field_6_caption));

        // GET PROPERTY CLASS FOR ICON
        $api_result = $api->load_local_api('listing__read', ['listing_id'=>intval($listingID),'fields'=>['listingsdb_pclass_id']]);
        if ($api_result['error']) {
            //If an error occurs die and show the error msg;
            die($api_result['error_msg']);
        }
        $pclass_list[] = $api_result['listing']['listingsdb_pclass_id'];

        // SET ICON FOR PROPERTY TYPE
        $iconpng = $pclass_list[0];

        //// END SET ICON FOR CRIME TYPE

        // BUILD THE INFO WINDOW WITH ALL THE DATA
        $infowindow = '';
        $customurl='';
        if ($use_customurl == 1) {
            $customurl = $config_customurl;
            $customurl .= '&';
        }

        require_once $config['basepath'] . '/include/core.inc.php';
        $page = new page_user;
        $seourl = $page->magicURIGenerator('listing', $listingID, true, false);

        if ($show_photo == 1) {
            // 10/29/2015 The printer_friendly GET var check does not actually work in the popup window. The <a>nchor tag originally wrapping the $thumb image was removed.
            $infowindow .= '<div class="tmap_info_image" style="width:' . $displaywidth . 'px; float:left;" >
								' . $thumb . '
							</div>
							<div class="tmap_info_text" style="width:170px;padding: 5px; float:left;">';
        } else {
            $infowindow .= '<div class="tmap_info_text" style="width:170px;padding: 5px;">';
        }
        if ($show_title == 1) {
            $infowindow .= '<strong class="tmap_info_title">' . $title . '</strong><br />';
        }
        if ($show_address == 1) {
            $infowindow .= '<span class="tmap_info_address">'.$display_address.'</span>';
            $infowindow .= '<br />';
        }
        if ($show_city == 1) {
            $infowindow .= '<span class="tmap_info_city">'.$city.'</span>';
            if ($show_state == 1) {
                $infowindow .= ', ';
            } else {
                $infowindow .= '<br />';
            }
        }
        if ($show_state == 1) {
            $infowindow .= '<span class="tmap_info_state">'.$state.'</span>';
            if ($show_zip == 1) {
                $infowindow .= ' ';
            } else {
                $infowindow .= '<br />';
            }
        }
        if ($show_zip == 1) {
            $infowindow .= '<span class="tmap_info_zip">'.$zip.'</span>';
            $infowindow .= '<br />';
        }

        if (isset($text_0) && !empty($text_0) && !empty($field_1_value)) {
            $infowindow .= $text_0;
            $infowindow .= $field_1_value;
            $infowindow .= '<br />';
        }
        if (isset($text_1) && !empty($text_1) && !empty($field_2_value)) {
            $infowindow .= $text_1;
            $infowindow .= $field_2_value;
            $infowindow .= '<br />';
        }
        if (isset($text_2) && !empty($text_2) && !empty($field_3_value)) {
            $infowindow .= $text_2;
            $infowindow .= $field_3_value;
            $infowindow .= '<br />';
        }
        if (isset($text_3) && !empty($text_3) && !empty($field_4_value)) {
            $infowindow .= $text_3;
            $infowindow .= $field_4_value;
            $infowindow .= '<br />';
        }
        if (isset($text_4) && !empty($text_4) && !empty($field_5_value)) {
            $infowindow .= $text_4;
            $infowindow .= $field_5_value;
            $infowindow .= '<br />';
        }
        if (isset($text_5) && !empty($text_5) && !empty($field_6_value)) {
            $infowindow .= $text_5;
            $infowindow .= $field_6_value;
            $infowindow .= '<br />';
        }
        if (isset($text_6) && !empty($text_6)) {
            $infowindow .= $text_6;
        }
        // ADD THE LINK TO THE LISTING
        // WE SHOULD CHECK TO SEE IF printer_friendly=yes TO SEE IF WE NEED TO USE THE to_old_win JS function or not
        // 10/29/2015 The printer_friendly GET var check does not actually work in the popup window. This conditional was moved to the JS code in tmaps.js
        // and replaces the TLINK text in the DOM..
        /*
                if (isset($_GET['printer_friendly']) && $_GET['printer_friendly'] == 'yes') {
                    $infowindow .= '<br /><a class="tmap_link_info" href="#" onclick="to_old_win(\'' . $seourl . '\');window.close();return false;">' . $lang['tmaps_view_listing'] . '</a>';
                } else {
                    $infowindow .= '<br /><a class="tmap_link_info" href="' . $seourl . '">' . $lang['tmaps_view_listing'] . '</a>';
                }
        */
        $infowindow .= 'TLINK';
        // CLOSE THE INFO WINDOW'S DIV's
        $infowindow .= '</div>';
        // AREA MAP - CHECK IF THE MARKER WE ARE ADDING IS THE MARKER FOR THE ORIGINAL LISTING, IF SO SET THE CENTER.PNG ICON INSTEAD OF THE DEFAULT

        if (isset($_GET['latlong_dist_lat']) && $_GET['latlong_dist_lat'] && isset($origional_marker_lat) && isset($origional_marker_long) && $origional_marker_lat != '' && $origional_marker_long != '' && $origional_marker_lat == $lat_value && $origional_marker_long == $long_value) {
            $iconpng = 'center';
        }
        //$infowindow='<![CDATA['.$infowindow.']]>';
        //$infowindow = htmlentities($infowindow, ENT_QUOTES, 'UTF-8');
        // CHECK FOR LAT/LONG DATA, THEN ECHO THE XML IF WE'RE GOOD TO GO
        if (($lat_value != '0') && ($long_value != '0') && ($lat_value != '') && ($long_value != '')) {
            $markers[$a]['lat']=$lat_value;
            $markers[$a]['lng']=$long_value;
            $markers[$a]['label']=utf8_encode($listing_pages->get_title($listingID));
            $markers[$a]['icon']=$iconpng;
            $markers[$a]['html']=$infowindow;
            $markers[$a]['seourl']=$seourl;
            $markers[$a]['view_listing_text']=$lang['tmaps_view_listing'];
            //$markers[$a]['infowindow']=$infowindow;
            //echo"\t<marker lat=\"" . $lat_value . "\" lng=\"" . $long_value . "\" label=\"" . $a . "\" icon=\"" . $iconpng . "\" >" . $infowindow . "</marker>\n";
            $a++;
        }
    } // END OF FOR EACH LISTING ID ARRAY
    //echo"</markers>\n";
    echo json_encode($markers);
    // WE'RE DONE! (wasn't that easy? uggghhhhh)
