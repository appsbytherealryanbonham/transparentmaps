<?php

function google_geocoder($address, $address2, $address3, $address4, $city, $state, $zip, $country, $apiKey)
{
    //first, build the address string
    global $config, $geodelay;
    $geocode_address = $address;
    if ($address2 != '') {
        $geocode_address .= ' ' . $address2;
    }
    if ($address3 != '') {
        $geocode_address .= ' ' . $address3;
    }
    if ($address4 != '') {
        $geocode_address .= ' ' . $address4;
    }
    if ($city != '') {
        $geocode_address .= ' ' . $city;
    }
    if ($state != '') {
        $geocode_address .= ', ' . $state;
    }
    if ($zip != '') {
        $geocode_address .= ' ' . $zip;
    }
    if ($country != '') {
        $geocode_address .= ' ' . $country;
    } elseif ($default_country != '') {
        $geocode_address .= ' ' . $default_country;
    }
    
    $ret['longitude']='';
    $ret['latitude']='';
    $ret['encoded'] = 'no';
    $ret['error_text'] = '';
    $newstr = str_replace(" ", "+", $geocode_address);
    $newstr = urlencode($newstr);
    $q = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $newstr.'&sensor=false&key='.$apiKey;
    // Parse the returned XML file
    $link = curl_init();
    curl_setopt($link, CURLOPT_URL, $q);



    //curl_setopt ($link, CURLOPT_POSTFIELDS, $array[1]);
    curl_setopt($link, CURLOPT_VERBOSE, 1);
    curl_setopt($link, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($link, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($link, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($link, CURLOPT_MAXREDIRS, 6);
    curl_setopt($link, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($link, CURLOPT_TIMEOUT, 60);
    //Slow down queries so that we do not get 601 (rate limit) errors.
    if ($geodelay < 1000) {
        $geodelay = 1000;
    }
    $geocode_pending = true;
    $retry=0;
    while ($geocode_pending) {
        usleep($geodelay);
    
        $results = curl_exec($link);
        // if (0 < curl_errno ($link)) {
        //  curl_close ($link);
        //  echo 'Curl Error'.curl_errno($link);
        // }
        $result_array = json_decode($results, true);
        //echo '<pre>'.print_r($result_array,TRUE).'</pre>';
      
     
        if ($result_array['status'] == "OK") {
            curl_close($link);
            $geocode_pending = false;
            $ret['longitude']=$result_array['results'][0]['geometry']['location']['lng'];
            $ret['latitude']=$result_array['results'][0]['geometry']['location']['lat'];
            //list($ret['longitude'], $ret['latitude'], $ret['altitude']) = explode(",", $xml->Response->Placemark->Point->coordinates);
            //echo "Address " . htmlentities($geocode_address,ENT_COMPAT,$config['charset']) . " geocoded.<br />";
            $ret['encoded'] = 'yes';
        } elseif ($result_array['status'] == 'OVER_QUERY_LIMIT') {
            $geodelay += 100000;
            //echo "GeoDelay Increased to ".$geodelay."<br />";
            if ($retry>2) {
                curl_close($link);
                //echo "Address " . htmlentities($geocode_address,ENT_COMPAT,$config['charset']) . " failed to geocoded retry limit exceded (Resulr Array: ".print_r($result_array,true).").<br />";
                $geocode_pending = false;
            }
            $retry++;
        } else {
            curl_close($link);
            $geocode_pending = false;
            switch ($result_array['status']) {
                case 'REQUEST_DENIED':
                    $error_text ='Indicates that your request was denied.';
                    break;
                case 'INVALID_REQUEST':
                    $error_text ='Generally indicates that the query (address or latlng) is missing';
                    break;
                case 'ZERO_RESULTS':
                    $error_text ='Geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latlng in a remote location';
                    break;
                case 'UNKNOWN_ERROR':
                    $error_text ='Indicates that the request could not be processed due to a server error. The request may succeed if you try again';
                    break;
                case 'OVER_QUERY_LIMIT':
                    $error_text ='Indicates that you are over your quota.';
                    break;
                default:
                    $error_text ='Undocumented Error Code';
            }
            if (isset($result_array['error_message'])) {
                $error_text .= 'Geocoder Error Message: '.$result_array['error_message'];
            }
            $ret['error_text'] = $error_text;
        }
        //echo "Address " . htmlentities($geocode_address,ENT_COMPAT,$config['charset']) . " failed to geocoded (Result Array: ".print_r($result_array,true).").<br />";
    }
    return $ret;
}
