<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('tests')
    ->exclude('vendor')
    ->in('src')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
        '@PSR2' => true,
        //'@PHP74Migration' => true,
        //'strict_param' => true,
        //'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
