# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.4.0] - 2022-08-13

More updates for Open-Realty 3.6.0 UI.

### Changed

- Finished updating TransparentMaps Admin Form to match look & feel of Open-Realty 3.6.0.
- Update geocode_all UI.

## [3.4.0-beta.1] - 2022-07-06

First beta update to support new admin interface in Open-Realty 3.6.0.

### Added

- Change for Open-Realty 3.6 UI

### Changed

### Fixed

## [v3.3.3] - 2021-12-23

### Fixed

- Set minimum PHP version to 7.4.3

## [v3.3.2] - 2021-11-30

### Fixed

- Fix Google Geocoder

### Changed

- Add better output on geocode failures.

## [v3.3.1] - 2021-11-22

### Changed

- Supports PHP 8.0+

## [v3.3.0-beta.1] - 2021-02-27

No changes since 3.3.0-beta.1

## [v3.3.0-beta.1] - 2021-02-20

### Added

- PHP 7.4 support

### Changed

- Project is now released under MIT license.
- Project is housed on Gitlab
- # This is release is based on the TransparentMaps 3.1.x releases, some features introduced in 3.2 have been removed, due to license changes.
